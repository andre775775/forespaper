<?php include('header.php'); ?>

<div id="cart-pix">
    <section class="title bg-full text-center">
        <div class="container">
            <h1>Pagamento PIX</h1>
        </div>
    </section>

    <section class="infos">
        <div class="container">
            <div class="pix-header">
                <h2>Informações para pagamento PIX</h2>
            </div>
            <div class="row">
                <div class="col-12 col-md-5">
                    <div class="content">
                        <div class="page">
                            <p>Aqui está seu QR Code para fazer o pagamento no valor de <strong>R$99,90</strong>:</p>
                        </div>
                        <div class="qr-code">
                            <img class="lazyload" data-src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQ4AAAEGCAYAAACU4nvIAAAABHNCSVQICAgIfAhkiAAAIABJREFUeF7tnQe0NUtRhbdZQYUnWYkigomcJPPIAhJEBASJkiQnAUHyI4MgTwQFCYokRREEBRM5R0EyEiTnoIgY1vefub8/51V1n+45M3PuvbvW+tdjMd09Pbt79u0zVbXrOyT9r2xGwAgYgQYEvsPE0YCWmxoBI3AEgWOJg/9tMwJGwAiUEDjyC8XE4U1iBIxACwImjha03NYIGIEjCJg4vBGMgBFoRsDE0QyZOxgBI2Di8B4wAkagGQETRzNk7mAEjICJw3vACBiBZgRMHM2QuYMRMAImDu8BI2AEmhEwcTRD5g5GwAiYOLwHjIARaEbAxNEMmTsYASNg4vAeMAJGoBkBE0czZO5gBIyAicN7wAgYgWYETBzNkLmDETACJg7vASNgBJoRMHE0Q+YORsAImDi8B4yAEWhGwMTRDJk7GAEjYOLwHjACRqAZgcWIA4Hk4yT9QPOUd7PDZyV9s2Nq3yfpFJK+J+j7LUlflvSNZNwfHPpGl/9T0uc65lPrwnqxbj1q+MyHebVaCaPSWP8j6euSvpI0AvPTtk5mR9v/h6QvSeKZ57DFiINNfztJF5vjKWe4xz0lvbvjPheU9GuSzhr0/Yykp0h6bTLutSTdNLn2Tkm/1TGfWpdLS7pDQnS1vr8t6W21RsH1C0i6haQfa+wLkb9Q0jOSfmeT9LjGMXe1+RslnSjpCzNNcDHi+BFJT5J0nZkedOrbXFLSqzpuclVJJ0g6d9D3I5LuLOkFybh3lfSo5No/SDq+Yz61LjeQ9FRJnAJa7YqSXtbaSdJVJD1W0jkb+/77QAz3TvqdV9JbG8fc1eZ/Jek2kv5tpgmaOLYEtImjDqSJo45RbwsTRy9yC/czcdQXwMRRx6i3hYmjF7mF+5k46gtg4qhj1NvCxNGL3ML9TBz1BTBx1DHqbWHi6EVu4X4mjvoCmDjqGPW2MHEMyL1G0st7UZyo390knSwZewriwC//0oKb9zsl8S+yD0l6enLthyX9qqRTB9eJHflbSW9O+pa8KsRL/NEQexJ1Zz7MK7JLSbqEpO8KLhIzwnNGsSOnknQFST8Z9BvjVSEu4vGdcScTbT/hCudfZCaOAZVHSrrHVCvQOS4BTGzUyKYgjto0ezE640AOPxXcgAAtXMBP7CAOXIHE5Xy0NvHg+v0lEQsTuXn/epjT+4J+EMajJV1ty8RBPARxHlnwWMcjju7yAEnEwpg4CnEcvS/F6NUpDGDikEonDhPHlLtPMnFIqgWAmTjqm7AXI584VtiWAsB84ijvv50NAOt9KeqvW38Lnzh84ujfPeN7+sThE8fRXVQKOa9ttV5y9YnDJ47a3qpd94mjhtAx133i8ImjYbtsvalPHCNPHKRCk1Uaue7GrBbEkGWiMu4UxMFz3DDJjiWF/Wcl/WjyUEucOHCb3j7JjsX9+SJJX+1YhPMPiX7Rmn5M0usTNy8ZszeTdKGZvSqs2xk6nrPW5Q2SPp00MnGMJA789n9SiKmoLU50naMXGaW/OLNXhRcFXYgoHuPMkh4m6Ro7RBzMN8uMPb2kF0s6U8cCPHyIm/ivoO/lJT1I0tmDa+AGft89M3E8U9I1O3VJSvBcVxLu58hMHCOJ48qSni/p5B0bNOsCcRBwRnRjZlOcOEqPcBZJj5F07R0ijtJ8ORm9Ojk91ZaK+ATIIxJDWiKtvuZVea6kXyoE4NWeN7vOHy6CuUwcCQhj3LEmjhWoS/xUMXGsEDBxDMetI19JJzh6ZRvNxFH/m+QTxwojnzhWOPinin+q1FlDkonDxHHsRjFxmDhMHGsI+BvHChB/46i8GofppwrPyr/Ic4IKd6aQzsdGEr+yD7YIGfOdo9VqAWB4cp7VOuiQAMgHzkw5nPtmH7RLxIEL+F5D0lnLtFCHJyMXvdLIxoSc+xuHv3Ec2VNTelVuLukmyUvzSkm/K+kDwc7GzUicwimTjY8K+ida3qShbYk4kNcnWa2ntAIyAA+R9PlkTmTcXjS5ViKOHxpw+P7GZ+VZIOVPmjgakas335eRo/vNq0KZgrsnNVAImEKFm3IGc1mJOMbMoZYdi84HMTiRlYhjzJxKfX3i6EfWxDFgN+WJw8SxAtnEUX9R/Y2jgtFh+sZh4jBx1Clj1cLEYeI4ioCJw8Rh4tgUAROHiWNtD/inSv3l8YnDxHEUAUSO7yQJ78C6IUZMYE9P3Vm8LlnC2X9LQnA3sjEfR0lCw825F2l87PifGhLyPp7c99mS0GaNjFKW1D6NclUQUEYLNbpn/VXMW/jjaD96/jg6YDflx1FSvkkbj17yD0tC0T1zYZaW9rKFzNn3Dy/itonjTUOMB8S0bmTO4j6OMlVpi1s0I7PjBrdzpGQOqVI/F9fqNs3E0Y+miWMG4uhfnnLP3qLTY04cyBmgfxGdDGrZsaW6Kr0q52OwNXH0o2fiMHE07R4TxwouR446cvTIRpjyp0rTm9nQ2CeOBrCCpj5x9OPnE4dPHE27xycOnzj2/tAeKa1nPY7dUwCrvdE+cdQQKl/3iaMfv3174njeBJqjfyfpSgUsx0gHRt6CTZat5IKEOB6RDPKPktDqjIyPo7iBz5VcZ67ZfMeeOLJ6wHwc/c2OEpCbYJi1GUMczxmkA3vXNZsTOqaWDiys6piQcxaclyaLYejZTLygJJo9eALi4AU9Z6IMXporquFvK2TAXl3SjZIByJyFPCJDPZ3ixRSfXjdS/88j6SeSvmOIA8HhdyTjItLLS0NsyrqVasf2rPVenzHEQX1dMn23TRz8IcDlHZmFfEYK+YzZLGP69p44SiHnpfl8ZCi2TAxDq11mUG2P+lFugCzV9wYXv3fQr7jtBMTR+gx77XeROHqfZUw/E4eJY6P9Y+JYwWTiWOFg4jBxmDg2QsDEcSxMJg4Tx0avjU8cJg4Tx9qrMubj6EZv3QSN/I1jVUGvN+S8d0n8U8U/VY7uHRNH/TXyicMnDp84Gk8cj5OEDuUuGZmsEF5kpIu/KrlW8qrspakjrLtupKfjwnxJMi6p5vyLbBe9Kii6kyIfGa51/kXuzdKJA/cxIsZZRm4Jo5I79otDEeyv7NAGRJuWWJfIiP24zSA0PceUdzYAjPTrLAV7DmCie6A2HpU4oG0vcRCIBTlEehykqEM6WVDaEyTdbx8RBzVwKewdGWUgiI3AHbxuJeIg3gT8ohIS6Ib8/nA9umeJOCDyL0+gATJm7xJ/wz8Th6TrjEFyh/r2EkdJ5XxMJbddPHFMkVb/k5IeLelqwV4ggI6TK3+pW4ljh7bWRlPxiWMjmHavkYlDWkKPw8SxehdMHLvHCRvNyMRh4thoo0zUyMQxEbBTD2viMHFMvcdK45s4lkR/xL1NHCaOEdtndFcTx2gIlxnAxGHiWGbnHbJvHPjdcaWdekm0t3hv0sUzn38pjgN183+VRIzDulHEGVfku5J5UquV2JLISl4VYhuYb+TuJo7i7FqRQGSk65NVG+mE8PxPlkQMRGQ3LaTro4XyiiTOA0mCq0g6TTAo7skfl3Sq4FrNq3JySefb4h5YcqgvDIXLIxHpKea1WBzHFA+zq2PuWlr9VDhNVXQa0njsoGnSMvcacbSM5bbfjoCJY4YdYeJYgdxbyc3EMcMmbbyFiaMRsJ7mJg4TR8++2eU+Jo4ZVsfEYeKYYZvNegsTxwxwmzhMHDNss1lvYeKYAW4Th4ljhm026y12ljiIi+CjWKvh3ny+JLQstm33LWQn4obErRrZxSRdJFFlRwGd7NfTBx3JzsRNGYkK0xz3Jdm1kZ1Z0g2Sa6ib31zSaTsAQnUdfKOi07hycXFmGcTXk3S25J5IKDw8qUmLu/VyicsVd/4vSPqpYNyaV+UMkm7fgQFdniXpnwvrTWkK0v3X7ROSni0JUaj9bDtLHHeS9JAOZD8k6VZDBfiO7sUuqINnehyohlN1PjJiVvgXaU1AjmhJ/mzQkdRu9Dqil5TmvzOk3Uf35OXNykeQrk9WLvERrcamv3XygvMiEndypmRQ5kNF+8hKxMGzUDYhIqRzSHroQB7r49aI49ySXtsKwND+hpL+IolnQReDPzKnCMYmfubXJL2/87670m1niePuhWJDJfA+KOkmBVGdMcD3SgeW7nlVSScMojGtc3ukpHu0dpK0VLX60lRLxFHqt1R27C9L+rOEOO4w/DFAv2Xd3iqJk9f7OtZtl7qYOBpWw8QxneaoiaNhI+5AUxNHwyKYOEwce9vFJ47ht/fcRadr76t/qtQQkvxTRfJPlfo+maKFTxwNqPrE4ROHTxwrBA4VcfBFH09C5hKscQgfXSMXG/1Qn44Eh7lGFjAZp5ES94Uk3VjSWWs3D66XThxki1IUOTJcpmh0HhdcxHPx04X5lOqq4HGiaPLpkvviko4yXGnubxwr0C5YwK+0RT49FE3PVO87tlexy6EijpNJws2LO6zHiIv4WtIRV22myo77kriJHwr68hLzMmWu09I8S8RxYUnPSDp/dlBHp/zCuqEyfq9CDEiJOCBGXLFRxXnu81RJF0/mZOJYAXPiELPSuj9Rj0fxHtmDOexQEccPSnqwpDt2Istf1ExrojRkb+RobZol4lhC5bw2397s2NK4B+0bB7Eh16gBGVw/NApgNWym+Dhq4lihzumIgLUoIpUTB9oXt00WqHTiqK2piUOqxXGYOGq7qHLdxFEH0CeOg+dVMXHU932xhYmjDqCJw8Sxt0v8U2VAwsRh4qgjYOIwcaztErwQkEerfXIo+xe5RhG2JTnp6q2DDu17vSpLfBwteVXA6GaJ0DEekZJXhb9slFQk+W7d8KqQlZsVgH6gJOq1RvZ4SX+QjFtarjEfR8lM5qdBZLilycrNkvJKkaM3kkS+SuRFIwmTkyL6rJGxV3BbR4aLPRP39oljQIysxyhjtPbOs1g/n6SMs9nfLuk9tUGS62yWzG16H0n/UtgMkGCULdk5lSPdSj9V2GBIE0SG2/hViWuZDF6yRnlpIuPD6puT5C68TmQ0Z+n6SARAWpGBXaaeXsJoDHGwHscng/MhnYLVuPAjKxEHMTuk+UduaTDCJZ3thX+UFLnJmQPJcddN5mPiGPMmDRv+aclLg3w/6c78deux3sjRJU4cPc83ts+Y2rG99x5DHKV7ktmKvkr2gpeIozQuJy6kCTJJg18c6sBGYyC/QLxLZCaO3h009OMvpYljJIid3U0cdeBMHHWMFmlh4lgE9iM3NXHUsTdx1DFapIWJYxHYTRwbwm7i2BCouZuZOOZG/P/v5xNHHXsTRx2jagvcdpmwLTqbmdZmaWCI4w8lIRC8biSokQhEIlGP4RGIMkoZ68oFzdF7SrqrJESC1w1dUZ6T/64bHo4SRr16HD3PvteH9cJFGemnojn6ssElG90DmUQ8K622h0N0T7xviBwz9rqhOfqE4YN46z35KIouaPZx9PqSXpB4l0r3+jlJTy/U0L3OoAcbjcGHUVzhkaH1ejtJiCHPYYsluZGeTmp3pE7NgyMi+/IOBHixrynpLEFfUo7/XtLrO8alC6nzxIJExosfFWKm7aUHF1yUko/2JC9TlNXIpkUBPcNoCeLARc6X/yi+gRc7+0MADmTrEsfQasgSXLGgcg5GkZcC9/vrBjJrvSfrfOeCjMJzBhmFbM2z+9Uw+tOCqj2Ji/yLDFc2AtRfbX3QzvaLEQf+7CdJgmEjW+Kl6MTwSLd/knSpjgFYbP6KvDPoC/k9RtK1dwgjguBIj4/iWWpFpzvgOdKlt3Zs7/2m7FdLcpvy3tsc28SxJTRNHKtoSH4ifnRLmO4NY+LYMqBbGM7EsQUQfeJYgWjiqG8mnzjqGBVb+KfKCh7/VKlvJJ846hjN3cInji0h7p8qPnFsspV84tgEpUIbnzh84th0C/nEsSlS87Xb2RMHiUB4XfaLoVQeuYCZP/9/lm6O25m4k8hNiVo4En6Zt2YJzxPFnyk7GWV+4grE40Kx7MgoWP2ljgVFIuA3kpq0ZK+SjZqlm3fcbqMuFJxG9DkyavOipB9hVDtx4O7OnuXDExVT3+iB1xrtLHH0PMySfUhhJ1U9MmfHrlAhFoMAsW1aKTt2m/dZH2uqSm4l6UACwB405UM1jG3iaACr1NTEsXtJblta2nAYE8dCJSBr3zimXPQpxjZxmDj29tWYavU+cVTeThPHePpa4htHada7luQ2HuF8BJ84fOLYyv7yicMnDp84tvIqlQfxiWM8yD5xlFXOxyPsE0eGwL78OEq6NK6wKDORNHTcWVlx6DGbiRyMKP2dMfldixByZKhek4ZNndh1IwuTjF4qqLXaVMTBfCKFbub3dUmfTyY65qcKmcA9Ys64uXFZI1DdamDOnCND6oAQ+my9uedLkz045hvHk4cqe9GccHWTkh8ZAtRf6JSiaMWN9vuSOEiNv2VS5JmYCUoYZhLzPSDt9fmZQhwCMSfnSwZnwZ85vHTrTS4vCb2OLHW+NN8piIPYg/tLIi4lMjQo7pKUMRhDHIyJnkQrgX5kKIeRucJL+LGelKWMjHiTixbS1HlJv5H0HUMcnMSzP3rs+Vsk98TNTVmLT43Z4A199yVxIFrCxylOHutWUgBrwCVsWio6XQo5p9A1Lzkq6+uGAM0JQ0mC1vlNQRxjaseOIQ7ICgLNyk9k2KBDgUjSi1vBG2q8EJAVGcRAEFe0ZrVbjSGO0thWOZc05huHiWO1vUwcq1osJg7p0JRHMHGsXn6fOFY4+MRRO8dIPnH4xHF0l5g4TBx1yli1MHGYOEwca2+LTxx1+jBxDC7I35N0rTpeJ2mBGwzty+jjKB+0yDalPue2DY/NF5NBX1K450MH7dBISJaUcYoxo37dauiRZqrXtbHwnkSq4XwcZb6/ngyAmO6tJH0zuI7KObVPsyxhRKTxiEVGaU6yblu9Kog9IyLNd6/ISmr51Mh9Q9IPrwqZqtnH0W9VXPO85JSRXDdc9hQ+Z96RIbCcCSCbOIYNQro0fvhWQwL+lYnPmmLBuNGywset9zq2/RWSVGnaoKbNl/jISMF+tyQ227rhiaDWRrTJanOlUHPmFSj15fsSHowohoG4Beaa6YZST5VnjeIbiEkhAzaKV2E+rHeWMg5x4h7NqsNnz8OL/Zak+jvkxh+Z5yadwfwXkmsQK4riUWo8Xag/nJFOyavCHnl1gZAeXVhTE0ftjdjR671Fp3ftcc44xC9EsSOUkKAswBMnmDQxE5DvXMaJ9HGdp7IxRadLxFF7dhedriG0D6+bOMYtmomjjp+Jo47Rvmth4hi3ZCaOOn4mjjpG+66FiWPckpk46viZOOoY7bsWJo5xS2biqONn4qhjtO9amDjGLZmJo46fiaOO0SQtlnLHIiJL3kRkuBl/uuDay4AghR1X48eSBnhFsozcTxcqw5e8KsQR/HGh4HfJHVtbUOItsniVXnds6Z54iP5GEgWiW63mjmVdskxUimBTExkF9nUb4479pULcE255srCzOKPW56+1Xyw7tjax3utLBYCVAnd4YUjE+uHGhyJlnKCov0z63UnSw5JrZOuygSMrEQcbohTcVAoAqz0eWilZNfveALDSPXmWUgBYqW8tAOwZkjgdRM9DLAr/ogC7MQFge+NG8yauJooTqq1J7/UDRxxLpdWXFqC3PALEQUwFGhiRQUaPSq79g6TjO4ijtpH+RNLNksjRWt/S9d6Q8zH3LPWtpdUTVMYJICPCbOxaXZWpnmfb45o4GhAt6XGYOBqADJqaOMbhN3dvE0cD4iYOySeO1YbxiWMhlfOG97WpqX+qrODyT5WmbXOSxv6pUsbPJ46G/eUTh08ce9vFJ46FThx8VCING/dpq+GmRIE6Sj8+05AWfv7WQTdoj1r515J2ZJOiNB0Zaeg3T56V5z9NIlC7ix9HkajjY2+UVr8BhGmT2w+p/FFaPZjjXo7uSXuKc0f7CE8DJ4esODSiwHjhIqNwNkLSkRQC7e8+KKtHnpMSDu8ZxJ5xa0dGweredwL38FyelcVOHGQf4krM0ppL4HMMv3XyouKy4mTQKnq7yaZHcyOT8OcL+xuTQSg1wL/oCzzV33HXRpmqu0gckHYmH7AJhlkbXNX8i15ENDxIKf9Q0JmfprhyecnXDSJ/iqSHJzclvoa0+8hw436yoI0BeaI9kmlnZM8J+YFf9oKjJYM0Qau9fMgCtsp5AbmSWHEr4C3tp4gcLUkH7iJxtOC1rbasN27pSPymVK2+llaPDkqPngnPVSoBOea5S7VjS+NarHgD1E0cK5CWiOPYYHm23sTEUYfUxFHH6IhMXFZXZYPu3U184uiGblRHE0cdPhNHHSMTx4CRTxzl2rH+qbLBy9TZZLGPo1PVVenEYaNuPnFsBNPWG/nEUYfUJ446Rj5x+MRxdJf44+gKChPHsCWeLYlCzpH9xPCNI1KgJr4D9xvK4uuGCjcJWrhO142j15uHbNSMuyi5kKleU6Q5KwtAmjrPE5VzOJUkXIqRMjiuXzJcz5VMiJgVsIiM9Oq3JddwVZ8nuSfuQspWZBm5JV5Hwfx3JZ0+aXS3AePo8k2GkhcRvqUTB2sKBuC4boyFsjou78iIl7hQco34jesnhcLpwv7iBBoZJT9umsRjfGAoh/HxpC8lGTIleDDKioGbOAZAS3VRryzp+cnG/6AkAI6ql7NRKAB9x4Q48IX3+NAZrrfodOlFhIionXLtUqMtXxujcj5V0ekScZQeH1JhrakT02q1kPPSeC463Yp2Q/vaNw4Tx+oEY+JYFVvK4jhMHCsEfOLwiePou2DiWEFh4ljh4IJMG9SO9YnDJ449BjVxmDiO/jX1T5X67zqfOHziOHaX+MThE0edNQYvjb9x+KfK3mYxcQxp5NQRxQ8fGZmmr0iulbwquCHJYsUtu26kYF9C0gWCa7hjSSp73kav9Ekbcd8s1RxXK/9aU7DJ0CSl/BvJnCiKfLXO+WbdyNok+zhz5ZIUhhYFc1u3MV6Vi0m6SJJBjPsYjxhix+tGyvyLJFGAe91oj7zCJTswwrv05MKaUnEeZfZoTbkn+yzK0P7M8CEzyzB+2lD0O5qyiWNApaR6XVKnLhFHSaWbRc6UounHP9TKe4w5vSbpiHYDiuSk1rcYZRHuJemFSSc8DZnKect9jm1bUwYnHuWWyQs1hjhYF2QHohcRFznPGcWskDGLLAF/LCJjzIhwavjU9sKzhlIF0XxLKud7auRZOj5lFSBCE0dthTqul4ijY7itdOGvWhQ7wuBLqJxv5aGCQUqao2OIozTfq0h6rKRzBo2oZUPOzouneuBk3F4FsNo0XZCphtCI6yaOFXilJLcR8Ba7mjhW8Jg4FpIOHLOxTRwmjr394xPHColDEwBm4qgjMEYBrD56XwufOHziAIHF0ur7tu2ql08cPnH4xPHtb9ChOXGQuYjbig9qrUahZTwVqFSvG+5Lslw/3zqopNMObrSsK4uTeV2oxI6rLTIyHhHGjTI/ySTlWiSCzHi4516XjHuNQrZk6fHJ0uVDbqTYjhfi3EPGbjQGLvITExwI6jthwDHqi3fkZcnEEGvGNR8JOrNPyDZFAXzdcLs/XdKbOta71IV1Rsg4ExXm+9LPJ16gsxeKjFOT9g0FtXwEkLPMWTKas6LdPD/Z5FMISUc4LXbi4EUha7FH5RzCoKRAtMk43rOoPRsJ3zsp8JmxcMjmR/b4IVU9ukaBYoo1R2n11HeFBCOVczYtGyEru4B7l5e11T4hiVIPZBKvG+T228P1aFzIJiNl3JAQYeb+LBEH7urbSorKI7DePGdEvLzgxNBkGLVis9eeF5z9kJXDIPWdavSRO5bs7LskRcbfJel2kj6cTOwhhT9eVKOHJCNjb7FXoviaXgxK/RYjjlrIee/DltLqa2MSkMbJIbNSQabetPqSynltvr3XiQ/hWfmwuG68uLg+eYm3bSXi2G+1Y0vYjEmrL6mcQ+gP2vaidI5n4jgGOBPH6i++iWP115tiTV/peLFMHB2gbdrFJ44VUj5xrHDwiWOFg08cFQYxcZg4jt0iJg4Tx0aHDhOHicPEcdJXxSeOCn3gVXngIMa7EdMc04iv2bjmIq9K6eMoX8D5Gn5ccEOukaHJl+vMEL7NvCp4YzLh2ydKoiZo9IWePng4zhrcFO8ELuIsOa5UUBm9zTMnD0JNVESbo1qseC4okN3j7WK+3DMTdC59HMXTQPJc5FUhsxgcomute2fT9qwzIsdZ0enSOKwn3zmohbtuZBdfLyln6Z8qG6wOmwufNBXrW420eLJGoziOEnHQnkUlkSgySOWihcn8TSGOA59+pLTNcO8fXJ9RTADu49cn/ndellsMhBZN688LRAe2mUgvrktiQyggvW6488jyxW3Yajz/I4bq8VHfEnEQw0H8Q/TH4IIDDmdsndCI9kgkUOy6p/o7fwRIyIsI1MQxYlHGdp1C5XzsnHr7k0J9b0nvDAaoKYCV5BXR6kBXo9WWUjkvzbOUHdv6fEu3N3EsuAImjhX4Jo4FN2HnrU0cncBto5uJw8SxjX20xBgmjiVQH+5p4jBxLLj9Rt3axDEKvnGdTRwmjnE7aLneJo7lsNflJZE4hlt23Ugeuk2SUYrg7X0k3XrmuSNay78oIYoMTERo3x3MCZfz/QYZgWjKJNaRvxAZCVqZdmXp8fEmMJ9ndmCEh4xcn8wNfM3CB1sEmTOxZ1IB8BBFmqN4YfCWRR4M8ikYMxN77njEo13Ye7ifWwWoxxAHCXB4rSIj2Y/nzLRMxzxr1HexXJUxD4LbjsLR0WYhU5L0dxK51o04gEsPhYjH3L+1L/dEkzRyH+OqxfsRpeTvCeYicBvZaweXYXSNlxfXc4/xQvQI/BJvQlHkUyY3RQQoywr9O0mvTLI7WW/II3J340LnBBoV5uZlwrWcpfL3YLPXh1gM5BDmJA5S7jNd2/cMotY9cSc9OOxL4uh50CX7TCVWPMUzTZnkVpovJ6eHF04dWV/iPx5xoluRAAALF0lEQVSdlIkg1fxxg7t721j1ao6OOXGUnuHQCPlseyF3eTwTR311TBwrjEoh5yaO+j46UC1MHPXlNHGYOOq75JC1MHHUF9zEYeKo75JD1sLEUV9wE4eJo75LDlkLE0d9wU0cJo76LhncffjlcacdBHtHQWYOt+j1h8LTLc+KYO5fd2aqttzn2LZ4VUhxv1bHACTIUaya/7Yacga4a1uzUcd4VUjXRzE/MuZB4fNM/LfXq4L7HdGijyb3pZh1JFxNc5Ie+RfZofGqTCXk07pht9W+VDu29x617NjecafqR6kCNE2yl2KK+44hjvNKwj0aWU1ztJc4ahi4dmwFIRNHbQut/ro8RtK16013ooWJY/wymDhMHKN3kYmjDqFPHCuM/FOlvld2soV/qkg+cYzfmj5x+MQxehf5xFGH0CcOnzjqu2SHW/jE4RPHNranTxwjTxwI6ka1VrexOL1joI4eiekyXi9xkOGLIjl1V9ftTJIeLOnqyYR7MeIZUOCO7knWIyLGmUsVdy3yBFFWaO2nCtmzWdYtz5Klv9OHNPZovrj0KYt4pQCjWpIbxcDJOI0MVziu2izbtNergpsXtfvMzYvkA5nCkd1xcJVH1xDSvpskFOznsMWyY2teFbIaM62JOYCJ7kFKeFbkuZc42PA8Jyna61bSmqBtL0bofJBMxTF/3SAMFOSfkoD8y0O1evRF1q1GHC+QdNlkXKrcU3qSVPh1I6We6+doxKhGHOALCUbGi1FKUe8lDuJ9KE1BPEdklNFADT4ysoBZ88ggJMj3wOtx1IijJMSbYDf5//25QgmEXuIYUwKyFyPKDCC4EwUa1VTObzCUZOghDu4JCURWihztVTmvEceYDdNLHGPS6l10evjL/SRJ10lWr/elGLMZan1NHJKJY7VLTBzD79W9I06rolHtZcuu+8SxQsYnjhUOPnGscHAJyAqjmDhMHMduEROHiWOjQ4iJw8Rh4jjpq+ITh08cGxGof6r4p8qxG8XEMSFxUOH9volq+EZva9CIbzxvGVyR2Ri79nH0XwtuPdLb75E8SMmrQnzB+yR9POlLGYeXSIqU14nDIBU9i8eY26vCs5Cp+4HeTVHoR1Hv0yXxLLidn57EIX1lqBOcxSiZOCYkDuTwn9ehb1GaEsTx8iSQaK/frhFHyWdPyYXLdRBHLQ4AzYxbdGpuzE0ctWfZBp9EzgRiLaiH8+WOG5g4JiaO509EHFnwDY+za8RRghjiOL6DOGp7HeIggCkrnlTqPzdx1J5lqut7gXmcLlrNxGHi2GjPjPnGYeLYCOLZG5k4JoR8jFelVDu2d8p7P1V84igj6BNHfYeZOOoYdbcwcayg84ljhcMUcRzdm3NkRxPHSABL3U0cJo5j94eJY4WGv3H4G8dGtFs6cfBx7RWSPrjRSN/eiOzLE5N+JXds7Va79lOFIuMUq46KWZOOjyDxxZOH+qykZyXXSOK7qaQoma+GUenEgao/SXtINER2c0nnTq45yW1kktth+cZBDAIp7i+s7dTgOjEMpFlHdpCIg5gT6ta8NHhQdE5uI+kBCQ5vl3SJ5Noph7IU6Ja0Wok4iP94miR0RCJjzmi0RGbiMHEc3RelE8dHJN1ZEgFF27SDRBzvlXRXSS9OiAPxm4cm4JHifv4CcRBgd4oO4EvEwQno2ZLO2TGuicPEYeJYe3F6v3GYODoYaAtddlYBrKTHcVh+qvjEsdrhJSEfE8cWWKBjCBPHANouxnGYOEwcx77T/qninyr+qeKfKs1/500ch4w4UCtHIDhS+D7XIE57+mAbfUrSEwaXbOsu46MeX/AjO80gDJwVMC7di3yThyWiwuiVIsabKaSXclUQR/7jpOg0HxL5iBwVKCd/iMLc7wkmjSsVaUqUwyPbbx9HwQiPTGTUuiULuCeHqHVv0d4/VWb4qcLGJaM0UtQmDf2ZkviCv24ofhNrgJx+q11Y0jOSThAYnpWeGAViSyC0KPP005KuL+kTyX1LxAEBsPmjcd80qK6jor5uEDJJd0gtrBuZq8RLQJQHgTjA6PPJs5DUSEbuZ1o3Smd7E8cMxEGcwd0T196LJN170GjoXMOw22UksZnmtFp5hBJxlObJiQK3NDEb61aq5FZ79v124ig9j2vHDugcJK+KiWO1qCaOVSRrbxyHieOQfeMwcZg49l56E0ftHFi5fpiS3EwcJg4Tx0jC2Otu4lgh4W8c9Q3lbxx1jPyNY4NvHJQRJEuTQsTbMr4S8zGRauGZ9UoHlk4cCP/yNfxdwU2ZE+61rEBx6dlLH0cRGsZlGgkOMyaFpbNEK2qUZu5WvCnonH4smRjJepmcIffjX6Th2Usc4Md8MxclSW5Z5izu7H8ZinO37rE/GFzWUe1ZCl1z/eytg1bas4/u4KLTUunjKP58KolH1cvHrAeuLtyjcxIH7jPCpqNNxjU22Ws6HqpEHLh48eRESua8vLeUdLXknpBrVhwadfPXFVTOL5jEYnCrG0q6bkJYvcQBwaFvkcU+4FrOsMVlDYZR7E1tOc4j6aIDAa+35Y8PmbzIAWzT2Cv88clIfZv3Yqx96Y7dNgibjjfFiaN07zEh5yXi4ETAqQ3CWjdOGxDDbZOJjdHjKD3r/SXdM4kt6SWOKYtOl56Fv/yk8pOav261otOb7sWl25k4GlbAxLH6idircm7ikEwcDS9c1HTMx9GRt+7ubuIwcWyyeXzi2ASlzjYmjjpw/qmywsg/Vep7Ze4W/qnSgLhPHD5xbLJdfOLYBKXONj5x1IHzicMnjvouWabFzp44cJNRy3WX7G6F2JFLSnpVMlkEcS+WeAxIGb/8UMB4vfuXhvyOKGW8hgsp8zdOGpW8Kri4KUqF6zQy4iy+M4m3qM2JQswfShpdahAOjlzspIuT5xJlhpaS3Mgufr2kv0/u+UlJv59c+/4hsY7/thqZyXi1or5jPo5edhg3mg8eMoLAIrd+6/w3ab+zxLHJ5HepTYk4SvOcqiBT6Z4l4qhhegNJT+1MyYeQXla7QeP1XcyOLT3CGOLAxYuYT2SOHG3cOLvS3MRRXwkTxzh3rIljg+zY+jbcrRYmjvp6mDhMHPVdUmlR+zg6+gYzD2DiqANu4jBx1HeJiWMjjPyNYyOY0kb+xrGCxt84xu2jxXr7xFGH3icOnzjqu6TSAuHeWw1ZhKMH24EB7psobdemRgnCX5V05lrDLV7HrfkgSZH4b+02uJZ/ozNrlHuigr5NO8Pgdr5Ax6AIRKMFGxmSDU/csnQD9+GeJBFmgs6lx/iVQbU9avNmSU8eBJ87oGjuspg7lniAkyepx81PsQMdSNFG+6HVSGOn0HBP+nbrvfbao8PBfDM9jtK4ZM+ybpFuRm0+vRiVxiWmhJecebUa68WcIuP50ORg/G0a90S1vgd79gn/IkNz5OuJSvw257831mLEMcXDeEwjYATmQcDEMQ/OvosROFAImDgO1HL6YYzAPAiYOObB2XcxAgcKARPHgVpOP4wRmAcBE8c8OPsuRuBAIWDiOFDL6YcxAvMgYOKYB2ffxQgcKARMHAdqOf0wRmAeBEwc8+DsuxiBA4WAieNALacfxgjMg4CJYx6cfRcjcKAQMHEcqOX0wxiBeRAwccyDs+9iBA4UAiaOA7WcfhgjMA8CJo55cPZdjMCBQsDEcaCW0w9jBOZBwMQxD86+ixE4UAiYOA7UcvphjMA8CJg45sHZdzECBwoBE8eBWk4/jBGYB4GTEMdp57mv72IEjMA+RuAzzB0p+CMMYjMCRsAIbIoAxHGEQWxGwAgYgU0R+D9z+Z0Y2eahBgAAAABJRU5ErkJggg==" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-7">
                    <div class="content">
                        <div class="page">
                            <p><strong>Você também pode clicar no botão abaixo para copiar o código PIX</strong> e fazer o pagamento no seu banco de preferência</p>
                        </div>
                        <textarea id="copy-to-clipboard" class="copy-to-clipboard" onclick="copyToClipboard()" oninput="auto_grow(this)" readonly="readonly" data-toggle="tooltip" data-placement="left" data-trigger="manual" title="Texto copiado para Área de Transferência">00020101021226860014br.gov.bcb.pix0126mateusmonteirom3@gmail.com0234K13 Agência Web, Pedido #1000000135204000053039865406100.005802BR5915K13 Agência Web6010Guarapuava6213050910000001363044BBC</textarea>
                        <div class="btn-default">
                            <button onclick="copyToClipboard()">Copiar Código</button>
                        </div>
                        <span class="obs">Observação: Pedidos sem pagamento em até 24h são cancelados.</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="resume">
        <div class="container">
            <div class="pix-header color-2">
                <div class="row">
                    <div class="col-12 col-md-7">
                        <h2>Resumo do Pedido</h2>
                    </div>
                    <div class="col-12 col-md-5 d-none d-md-block">
                        <h2>Totais</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-7">
                    <div class="list-prod" data-simplebar>
                        <div class="item">
                            <a href="product.php" class="thumb">
                                <img data-src="imgs/products/product-01.png" class="img-cover lazyload" alt="">
                            </a>
                            <div class="info">
                                <div class="box">
                                    <h2 class="name"><strong>Produto 01</strong></h2>
                                    <ul class="optional">
                                        <li>Quantidade <strong>1</strong></li>
                                        <li>Tamanho <strong>X</strong></li>
                                        <li>Cor <strong>Y</strong></li>
                                    </ul>
                                </div>
                                <div class="total">
                                    <span class="subtotal">Subtotal</span>
                                    <span class="price"><strong>R$1.499,90</strong></span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <a href="product.php" class="thumb">
                                <img data-src="imgs/products/product-02.png" class="img-cover lazyload" alt="">
                            </a>
                            <div class="info">
                                <div class="box">
                                    <h2 class="name"><strong>Produto 02</strong></h2>
                                    <ul class="optional">
                                        <li>Quantidade <strong>2</strong></li>
                                        <li>Tamanho <strong>X</strong></li>
                                        <li>Cor <strong>Y</strong></li>
                                    </ul>
                                </div>
                                <div class="total">
                                    <span class="subtotal">Subtotal</span>
                                    <span class="price"><strong>R$149,90</strong></span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <a href="product.php" class="thumb">
                                <img data-src="imgs/products/product-02.png" class="img-cover lazyload" alt="">
                            </a>
                            <div class="info">
                                <div class="box">
                                    <h2 class="name"><strong>Produto 03</strong></h2>
                                    <ul class="optional">
                                        <li>Quantidade <strong>1</strong></li>
                                        <li>Tamanho <strong>X</strong></li>
                                        <li>Cor <strong>Y</strong></li>
                                    </ul>
                                </div>
                                <div class="total">
                                    <span class="subtotal">Subtotal</span>
                                    <span class="price"><strong>R$149,90</strong></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-5">
                    <div class="pix-header color-2 d-md-none">
                        <h2>Totais</h2>
                    </div>
                    <table id="table">
                        <tr>
                            <td><p>Subtotal (4 itens)</p></td>
                            <td><span>R$149,90</span></td>
                        </tr>
                        <tr>
                            <td><p>Desconto</p></td>
                            <td><span>R$19,80</span></td>
                        </tr>
                        <tr>
                            <td><p>Frete</p></td>
                            <td><span>R$19,80</span></td>
                        </tr>
                        <tr>
                            <td><p>Total PIX</p></td>
                            <td><span class="big">R$1.999,90</span></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include('footer.php'); ?>