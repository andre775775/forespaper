<aside id="sidebar-filter">
    <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-1">
        <div class="col list">
            <h3 class="title" data-toggle="collapse" data-target="#ex"><span>Filtros</span></h3>
            <div id="ex" class="filter collapse show">
                <ul class="list-filter-items">
                    <li class="active">
                        <a href="#">Nome da filtro 1 lorem us<span class="qtd">(1)</span></a>
                        <span class="close" title="Remover filtro"><i class="fas fa-times"></i></span>
                    </li>
                    <li><a href="#">Nome da filtro 2<span class="qtd">(51)</span></a></li>
                    <li><a href="#">Nome da filtro 3<span class="qtd">(6)</span></a></li>
                    <li><a href="#">Nome da filtro 4<span class="qtd">(2)</span></a></li>
                    <li><a href="#">Nome da filtro 5<span class="qtd">(55)</span></a></li>
                    <li><a href="#">Nome da filtro 6<span class="qtd">(226)</span></a></li>
                    <li><a href="#">Nome da filtro 7<span class="qtd">(28)</span></a></li>
                    <li><a href="#">Nome da filtro 8<span class="qtd">(0)</span></a></li>
                    <li><a href="#">Nome da filtro 9<span class="qtd">(77)</span></a></li>
                </ul>
            </div>
        </div>
        <div class="col list">
            <h3 class="title" data-toggle="collapse" data-target="#marcas"><span>Marcas</span></h3>
            <div id="marcas" class="filter collapse show">
                <ul class="list-filter-items">
                    <li class="active">
                        <a href="#">Nome da marca 1<span class="qtd">(2)</span></a>
                        <span class="close" title="Remover filtro"><i class="fas fa-times"></i></span>
                    </li>
                    <li><a href="#">Nome da marca 2<span class="qtd">(5)</span></a></li>
                    <li><a href="#">Nome da marca 3<span class="qtd">(16)</span></a></li>
                </ul>
            </div>
        </div>
        <div class="col list">
            <h3 class="title" data-toggle="collapse" data-target="#sizes"><span>Tamanho</span></h3>
            <div id="sizes" class="filter sizes collapse show">
                <ul class="list-filter-items">
                    <li class="active">
                        <a href="#">36</a>
                        <span class="close" title="Remover filtro"><i class="fas fa-times"></i></span>
                    </li>
                    <li><a href="#">38</a></li>
                    <li><a href="#">40</a></li>
                    <li><a href="#">42</a></li>
                    <li><a href="#">120</a></li>
                </ul>
            </div>
        </div>
        <div class="col list">
            <h3 class="title" data-toggle="collapse" data-target="#colors"><span>Cor</span></h3>
            <div id="colors" class="filter colors collapse show">
                <ul class="list-filter-items">
                    <li><a href="#"><i style="background-color: #f6da3e"></i><p>Amarelo</p></a></li>
                    <li><a href="#"><i style="background-color: #ff0000"></i><p>Vermelho</p></a></li>
                    <li class="active">
                        <a href="#"><i style="background-color: #000000"></i><p>Preto</p></a>
                        <span class="close" title="Remover filtro"><i class="fas fa-times"></i></span>
                    </li>
                    <li><a href="#"><i style="background-color: #8b8b8b"></i><p>Cinza</p></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="btn-default">
        <a href="#">Limpar Filtros</a>
    </div>
</aside>