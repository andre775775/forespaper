<?php include('header.php'); ?>

<div id="product">
    <nav aria-label="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item"><a href="category.php">Categoria do Produto</a></li>
                <li class="breadcrumb-item">Nome do Produto</li>
            </ol>
        </div>
    </nav>
    <section class="product-head">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="slider-product">
                        <div class="row">
                            <div class="col-12 col-xl-2 order-2 order-xl-1">
                                <div class="slider-product-thumb slider-hidden">
                                    <div class="item">
                                        <img class="img-cover" data-lazy="imgs/products/product-01.png" alt="">
                                    </div>
                                    <div class="item">
                                        <img class="img-cover" data-lazy="imgs/products/product-02.png" alt="">
                                    </div>
                                    <div class="item">
                                        <img class="img-cover" data-lazy="imgs/products/product-03.png" alt="">
                                    </div>
                                    <div class="item">
                                        <img class="img-cover" data-lazy="imgs/products/product-04.png" alt="">
                                    </div>
                                    <div class="item">
                                        <img class="img-cover" data-lazy="imgs/products/product-01.png" alt="">
                                    </div>
                                    <div class="item">
                                        <img class="img-cover" data-lazy="imgs/products/product-02.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-xl-10 order-1 order-xl-2">
                                <div class="slider-product-img slider-hidden" id="zoom-gallery">
                                    <button data-src="imgs/products/product-01.png" class="item" data-fancybox="zoom-gallery">
                                        <img class="img-cover img-zoom" id="img-zoom-0" data-lazy="imgs/products/product-01.png" alt="" data-zoom-image="imgs/products/product-01.png">
                                    </button>
                                    <button data-src="imgs/products/product-02.png" class="item" data-fancybox="zoom-gallery">
                                        <img class="img-cover img-zoom" id="img-zoom-1" data-lazy="imgs/products/product-02.png" alt="" data-zoom-image="imgs/products/product-02.png">
                                    </button>
                                    <button data-src="imgs/products/product-03.png" class="item" data-fancybox="zoom-gallery">
                                        <img class="img-cover img-zoom" id="img-zoom-2" data-lazy="imgs/products/product-03.png" alt="" data-zoom-image="imgs/products/product-03.png">
                                    </button>
                                    <button data-src="imgs/products/product-04.png" class="item" data-fancybox="zoom-gallery">
                                        <img class="img-cover img-zoom" id="img-zoom-3" data-lazy="imgs/products/product-04.png" alt="" data-zoom-image="imgs/products/product-04.png">
                                    </button>
                                    <button data-src="imgs/products/product-01.png" class="item" data-fancybox="zoom-gallery">
                                        <img class="img-cover img-zoom" id="img-zoom-4" data-lazy="imgs/products/product-01.png" alt="" data-zoom-image="imgs/products/product-01.png">
                                    </button>
                                    <button data-src="imgs/products/product-02.png" class="item" data-fancybox="zoom-gallery">
                                        <img class="img-cover img-zoom" id="img-zoom-5" data-lazy="imgs/products/product-02.png" alt="" data-zoom-image="imgs/products/product-02.png">
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="info">
                        <h1 class="prod-name"><strong>Nome do Produto</strong></h1>
                        <span class="sub-title">Subtítulo</span>
                        <div class="rating">
                            <div class="star-ratings">
                                <div class="star-ratings-top" style="width: 80%">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </div>
                                <div class="star-ratings-bottom">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </div>
                            </div>
                            <div class="star-count">(55)</div>
                        </div>
                        <div class="btn-budget text-center text-sm-left">
                            <a href="https://api.whatsapp.com/send?phone=5542999999999" target="_blank" rel="noreferrer"><i class="fab fa-whatsapp"></i><strong>Solicitar Orçamento</strong></a>
                        </div>
                        <hr class="color-2" />
                        <div class="btn-wishlist">
                            <a href="#"><i class="fas fa-heart"></i><span>Adicionar à Lista de Desejos</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="description">
        <div class="content">
            <div class="container">
                <nav>
                    <div class="nav nav-tabs nav-pills nav-fill" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-description-tab" data-toggle="tab" href="#nav-description" role="tab" aria-controls="nav-description" aria-selected="true">DESCRIÇÃO</a>
                        <a class="nav-item nav-link" id="nav-info-tab" data-toggle="tab" href="#nav-info" role="tab" aria-controls="nav-info" aria-selected="false">INFORMAÇÕES TÉCNICAS</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-description" role="tabpanel" aria-labelledby="nav-description-tab">
                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                        <br>
                        <br>
                        At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
                    </div>
                    <div class="tab-pane fade" id="nav-info" role="tabpanel" aria-labelledby="nav-info-tab">
                        Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="comments">
        <div class="container">
            <h2 class="title small color-2 black">Avaliações <span>(0)</span></h2>
            <div class="msg-default">
                <div class="error">
                    <p>Ainda não possuí nenhuma avaliação</p>
                    <p>Seja o primeiro a comentar!</p>
                </div>
            </div>
            <!-- <div class="item">
                    <p class="tit">Ótimo preço</p>
                    <p class="name">Eduardo G.</p>
                    <div class="rating">
                        <div class="star-ratings">
                            <div class="star-ratings-top" style="width: 100%">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </div>
                            <div class="star-ratings-bottom">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="page">
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam</p>
                    </div>
                </div>
                <div class="item">
                    <p class="tit">Produto Excelente!</p>
                    <p class="name">Cássio42_cassio</p>
                    <div class="rating">
                        <div class="star-ratings">
                            <div class="star-ratings-top" style="width: 80%">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </div>
                            <div class="star-ratings-bottom">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="page">
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam</p>
                    </div>
                </div>
                <div class="item">
                    <p class="tit">Muito Bom!</p>
                    <p class="name">Matheus.magento</p>
                    <div class="rating">
                        <div class="star-ratings">
                            <div class="star-ratings-top" style="width: 20%">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </div>
                            <div class="star-ratings-bottom">
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                                <i class="fa fa-star" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="page">
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam</p>
                    </div>
                </div>
                <script>
                    // APAGAR QUANDO FIZER O AJAX
                    function r(f){/in/.test(document.readyState)?setTimeout('r('+f+')',9):f()}
                    r(function(){
                        $('.pagination .see-more').click(function(){
                            $('.pagination .see-more').addClass('active');
                        });	
                    });
                </script>

                <div class="pagination">
                    <button class="see-more">
                        <span>Ver Mais Avaliações</span>
                        <i class="fas fa-spinner"></i>
                    </button>
                </div> -->

            <h4 class="title small black">Comente também!</h4>
            <form action="" method="POST">
                <div class="form-group">
                    <input type="text" name="title-comment" id="title-comment" placeholder="Título" required="required">
                </div>
                <div class="form-group">
                    <textarea name="rating-msg" id="rating-msg" placeholder="Seu Comentário" required="required"></textarea>
                </div>
                <div class="form-group">
                    <span><strong>Avaliação</strong></span>
                    <fieldset class="star-cb-group" required="required">
                        <input type="radio" id="rating-5" name="rating" value="5" required="required" /><label for="rating-5">5</label>
                        <input type="radio" id="rating-4" name="rating" value="4" required="required" /><label for="rating-4">4</label>
                        <input type="radio" id="rating-3" name="rating" value="3" required="required" /><label for="rating-3">3</label>
                        <input type="radio" id="rating-2" name="rating" value="2" required="required" /><label for="rating-2">2</label>
                        <input type="radio" id="rating-1" name="rating" value="1" required="required" /><label for="rating-1">1</label>
                    </fieldset>
                    <div class="btn-default">
                        <button>ENVIAR</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <section class="related-products list-items">
        <div class="container">
            <h2 class="title black">Produtos Relacionados</h2>
            <div class="row row-cols-2 row-cols-md-3 row-cols-lg-4">
                <div class="col">
                    <div class="item">
                        <a href="product.php">
                            <div class="img-item">
                                <img class="lazyload" data-src="imgs/products/product-01.png" alt="produto-01">
                                <span class="see-product">Ver Produto</span>
                            </div>
                        </a>
                        <div class="box">
                            <a href="product.php" class="name">Produto 01</a>
                            <span class="subtitle">Desc. 01</span>
                            <div class="price-box">
                                <span class="price">R$99,90</span>
                                <span class="installments">
                                    ou R$110,00
                                    <br>
                                    em 10x de R$11,00 com juros
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="item">
                        <a href="product.php">
                            <div class="img-item">
                                <img class="lazyload" data-src="imgs/products/product-02.png" alt="produto-02">
                                <span class="see-product">Ver Produto</span>
                                <span class="new">Novo</span>
                            </div>
                        </a>
                        <div class="box">
                            <a href="product.php" class="name">Produto 02</a>
                            <span class="subtitle">Desc. 02</span>
                            <div class="price-box">
                                <span class="price">R$99,90</span>
                                <span class="installments">
                                    ou R$110,00
                                    <br>
                                    em 10x de R$11,00 com juros
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="item">
                        <a href="product.php">
                            <div class="img-item">
                                <img class="lazyload" data-src="imgs/products/product-03.png" alt="produto-03">
                                <span class="see-product">Ver Produto</span>
                                <span class="discount">50% <span>off</span></span>
                            </div>
                        </a>
                        <div class="box">
                            <a href="product.php" class="name">Produto 03</a>
                            <span class="subtitle">Desc. 03</span>
                            <div class="price-box">
                                <span class="price">R$99,90</span>
                                <span class="installments">
                                    ou R$110,00
                                    <br>
                                    em 10x de R$11,00 com juros
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="item">
                        <a href="product.php">
                            <div class="img-item">
                                <img class="lazyload" data-src="imgs/products/product-04.png" alt="produto-04">
                                <span class="see-product">Ver Produto</span>
                            </div>
                        </a>
                        <div class="box">
                            <a href="product.php" class="name">Produto 04</a>
                            <span class="subtitle">Desc. 04</span>
                            <div class="price-box">
                                <span class="price">R$99,90</span>
                                <span class="installments">
                                    ou R$110,00
                                    <br>
                                    em 10x de R$11,00 com juros
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include('footer.php'); ?>