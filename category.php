<?php include ('header.php'); ?>

    <section id="category">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    <li class="breadcrumb-item">Nome da Categoria</li>
                </ol>
            </nav>
            <div class="row">
                <div class="col-lg-3">
                    <div class="filter-order-mobile d-lg-none">
                        <button type="button" class="filter-mobile collapsed" data-toggle="collapse" data-target="#filters">Filtros</button>
                        <div class="order-mobile">
                            <select name="" id="">
                                <option value="" selected="selected">Ordenar</option>
                                <option value="">Ordem Alfabética</option>
                                <option value="">Menor Preço</option>
                                <option value="">Maior Preço</option>
                                <option value="">Mais Vendidos</option>
                            </select>
                        </div>
                        <div class="show-order">
                            <select name="" id="">
                                <option value="" selected="selected">Exibir</option>
                                <option value="">Exibir 1</option>
                                <option value="">Exibir 2</option>
                                <option value="">Exibir 3</option>
                            </select>
                        </div>
                        <a href="#" class="order">
                            <img src="imgs/icons/order-up.png" alt="Ordem Crescente ou Decrescente">
                        </a>
                    </div>
                    <div id="filters" class="collapse">
                        <?php include('sidebar-filter.php'); ?>
                    </div>

                </div>
                <div class="col-lg-9">
                    <div class="list-items">
                        <div class="head">
                            <h1 class="title">Nome da Categoria</h1>
                            <div class="btns d-none d-lg-flex align-items-center">
                                <div class="list-order">
                                    <a href="#" class="sort-type" id="list-item-line" data-toggle="tooltip" data-trigger="hover" data-placement="bottom" title="Lista">
                                        <i class="fas fa-list"></i>
                                    </a>
                                    <a href="#" class="sort-type active" id="list-item-grid" data-toggle="tooltip" data-trigger="hover" data-placement="bottom" title="Grid">
                                        <i class="fas fa-th"></i>
                                    </a>
                                </div>
                                <div class="select-custom-box">
                                    <span class="select-label" id="select-show-order">Exibir</span>
                                    <select name="show-order" id="show-order" class="select-custom" label="select-show-order">
                                        <option value="" selected="selected">Exibir</option>
                                        <option value="">Exibir 1</option>
                                        <option value="">Exibir 2</option>
                                        <option value="">Exibir 3</option>
                                    </select>
                                </div>
                                <div class="select-custom-box">
                                    <span class="select-label" id="select-order">Ordenar</span>
                                    <select name="order" id="order" class="select-custom" label="select-order">
                                        <option value="" selected="selected">Ordenar</option>
                                        <option value="">Ordem Alfabética</option>
                                        <option value="">Menor Preço</option>
                                        <option value="">Maior Preço</option>
                                        <option value="">Mais Vendidos</option>
                                    </select>
                                </div>
                                <a href="#" class="order" data-toggle="tooltip" data-trigger="hover" data-placement="bottom" title="Ordem Descrescente">
                                    <img src="imgs/icons/order-up.png" alt="Ordem Crescente ou Decrescente">
                                </a>
                            </div>
                        </div>
                        <div class="msg-default" hidden>
                            <div class="error">
                                <p>Nenhum produto adicionado nessa categoria.</p>
                            </div>
                        </div>
                        <div class="container">
                            <div id="list-box" class="grid-list row row-cols-2 row-cols-md-3">
                                <div class="col">
                                    <div class="item">
                                        <a href="product-2.php">
                                            <div class="img-item">
                                                <img data-src="imgs/products/product-01.png" class="lazyload" alt="produto-01">
                                                <span class="see-product">Ver Produto</span>
                                            </div>
                                        </a>
                                        <div class="box">
                                            <a href="produto-single-orcamento.php" class="name">Produto 01</a>
                                            <span class="subtitle">Desc. 01</span>
                                            <div class="price-box">
                                                <div class="btn-budget">
                                                    <a href="https://api.whatsapp.com/send?phone=5542999999999" target="_blank" rel="noreferrer"><i class="fab fa-whatsapp"></i>Solicitar Orçamento</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="item">
                                        <a href="product.php">
                                            <div class="img-item">
                                                <img data-src="imgs/products/product-02.png" class="lazyload" alt="produto-02">
                                                <span class="see-product">Ver Produto</span>
                                                <span class="new">Novo</span>
                                            </div>
                                        </a>
                                        <div class="box">
                                            <a href="product.php" class="name">Produto 02</a>
                                            <span class="subtitle">Desc. 02</span>
                                            <div class="price-box">
                                                <span class="old-price"><del>de R$120,00</del></span>
                                                <span class="price">R$99,90</span>
                                                <span class="installments">
                                                    ou R$110,00
                                                    <br>
                                                    em 10x de R$11,00 com juros
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="item out-stock">
                                        <a href="product.php">
                                            <div class="img-item">
                                                <img data-src="imgs/products/product-03.png" class="lazyload" alt="produto-03">
                                                <span class="see-product">Ver Produto</span>
                                                <span class="discount">50% <span>off</span></span>
                                            </div>
                                        </a>
                                        <div class="box">
                                            <a href="product.php" class="name">Produto 03</a>
                                            <span class="subtitle">Desc. 03</span>
                                            <div class="price-box">
                                                <span class="price">R$99,90</span>
                                                <span class="installments">
                                                    ou R$110,00
                                                    <br>
                                                    em 10x de R$11,00 com juros
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="item">
                                        <a href="product.php">
                                            <div class="img-item">
                                                <img data-src="imgs/products/product-04.png" class="lazyload" alt="produto-04">
                                                <span class="see-product">Ver Produto</span>
                                            </div>
                                        </a>
                                        <div class="box">
                                            <a href="product.php" class="name">Produto 04</a>
                                            <span class="subtitle">Desc. 04</span>
                                            <div class="price-box">
                                                <span class="price">R$99,90</span>
                                                <span class="installments">
                                                    ou R$110,00
                                                    <br>
                                                    em 10x de R$11,00 com juros
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="item">
                                        <a href="product.php">
                                            <div class="img-item">
                                                <img data-src="imgs/products/product-01.png" class="lazyload" alt="produto-01">
                                                <span class="see-product">Ver Produto</span>
                                            </div>
                                        </a>
                                        <div class="box">
                                            <a href="product.php" class="name">Produto 01</a>
                                            <span class="subtitle">Desc. 01</span>
                                            <div class="price-box">
                                                <span class="price">R$99,90</span>
                                                <span class="installments">
                                                    ou R$110,00
                                                    <br>
                                                    em 10x de R$11,00 com juros
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="item">
                                        <a href="product.php">
                                            <div class="img-item">
                                                <img data-src="imgs/products/product-02.png" class="lazyload" alt="produto-02">
                                                <span class="see-product">Ver Produto</span>
                                                <span class="new">Novo</span>
                                            </div>
                                        </a>
                                        <div class="box">
                                            <a href="product.php" class="name">Produto 02</a>
                                            <span class="subtitle">Desc. 02</span>
                                            <div class="price-box">
                                                <span class="price">R$99,90</span>
                                                <span class="installments">
                                                    ou R$110,00
                                                    <br>
                                                    em 10x de R$11,00 com juros
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="item">
                                        <a href="product.php">
                                            <div class="img-item">
                                                <img data-src="imgs/products/product-03.png" class="lazyload" alt="produto-03">
                                                <span class="see-product">Ver Produto</span>
                                                <span class="discount">50% <span>off</span></span>
                                            </div>
                                        </a>
                                        <div class="box">
                                            <a href="product.php" class="name">Produto 03</a>
                                            <span class="subtitle">Desc. 03</span>
                                            <div class="price-box">
                                                <span class="price">R$99,90</span>
                                                <span class="installments">
                                                    ou R$110,00
                                                    <br>
                                                    em 10x de R$11,00 com juros
                                                </span>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="item">
                                        <a href="product.php">
                                            <div class="img-item">
                                                <img data-src="imgs/products/product-04.png" class="lazyload" alt="produto-04">
                                                <span class="see-product">Ver Produto</span>
                                            </div>
                                        </a>
                                        <div class="box">
                                            <a href="product.php" class="name">Produto 04</a>
                                            <span class="subtitle">Desc. 04</span>
                                            <div class="price-box">
                                                <span class="price">R$99,90</span>
                                                <span class="installments">
                                                    ou R$110,00
                                                    <br>
                                                    em 10x de R$11,00 com juros
                                                </span>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="item">
                                        <a href="product.php">
                                            <div class="img-item">
                                                <img data-src="imgs/products/product-01.png" class="lazyload" alt="produto-01">
                                                <span class="see-product">Ver Produto</span>
                                            </div>
                                        </a>
                                        <div class="box">
                                            <a href="product.php" class="name">Produto 01</a>
                                            <span class="subtitle">Desc. 01</span>
                                            <div class="price-box">
                                                <span class="price">R$99,90</span>
                                                <span class="installments">
                                                    ou R$110,00
                                                    <br>
                                                    em 10x de R$11,00 com juros
                                                </span>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="item">
                                        <a href="product.php">
                                            <div class="img-item">
                                                <img data-src="imgs/products/product-02.png" class="lazyload" alt="produto-02">
                                                <span class="see-product">Ver Produto</span>
                                                <span class="new">Novo</span>
                                            </div>
                                        </a>
                                        <div class="box">
                                            <a href="product.php" class="name">Produto 02</a>
                                            <span class="subtitle">Desc. 02</span>
                                            <div class="price-box">
                                                <span class="price">R$99,90</span>
                                                <span class="installments">
                                                    ou R$110,00
                                                    <br>
                                                    em 10x de R$11,00 com juros
                                                </span>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="item">
                                        <a href="product.php">
                                            <div class="img-item">
                                                <img data-src="imgs/products/product-03.png" class="lazyload" alt="produto-03">
                                                <span class="see-product">Ver Produto</span>
                                                <span class="discount">50% <span>off</span></span>
                                            </div>
                                        </a>
                                        <div class="box">
                                            <a href="product.php" class="name">Produto 03</a>
                                            <span class="subtitle">Desc. 03</span>
                                            <div class="price-box">
                                                <span class="price">R$99,90</span>
                                                <span class="installments">
                                                    ou R$110,00
                                                    <br>
                                                    em 10x de R$11,00 com juros
                                                </span>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="item">
                                        <a href="product.php">
                                            <div class="img-item">
                                                <img data-src="imgs/products/product-04.png" class="lazyload" alt="produto-04">
                                                <span class="see-product">Ver Produto</span>
                                            </div>
                                        </a>
                                        <div class="box">
                                            <a href="product.php" class="name">Produto 04</a>
                                            <span class="subtitle">Desc. 04</span>
                                            <div class="price-box">
                                                <span class="price">R$99,90</span>
                                                <span class="installments">
                                                    ou R$110,00
                                                    <br>
                                                    em 10x de R$11,00 com juros
                                                </span>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="item">
                                        <a href="product.php">
                                            <div class="img-item">
                                                <img data-src="imgs/products/product-01.png" class="lazyload" alt="produto-01">
                                                <span class="see-product">Ver Produto</span>
                                            </div>
                                        </a>
                                        <div class="box">
                                            <a href="product.php" class="name">Produto 01</a>
                                            <span class="subtitle">Desc. 01</span>
                                            <div class="price-box">
                                                <span class="price">R$99,90</span>
                                                <span class="installments">
                                                    ou R$110,00
                                                    <br>
                                                    em 10x de R$11,00 com juros
                                                </span>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="item">
                                        <a href="product.php">
                                            <div class="img-item">
                                                <img data-src="imgs/products/product-02.png" class="lazyload" alt="produto-02">
                                                <span class="see-product">Ver Produto</span>
                                                <span class="new">Novo</span>
                                            </div>
                                        </a>
                                        <div class="box">
                                            <a href="product.php" class="name">Produto 02</a>
                                            <span class="subtitle">Desc. 02</span>
                                            <div class="price-box">
                                                <span class="price">R$99,90</span>
                                                <span class="installments">
                                                    ou R$110,00
                                                    <br>
                                                    em 10x de R$11,00 com juros
                                                </span>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="item">
                                        <a href="product.php">
                                            <div class="img-item">
                                                <img data-src="imgs/products/product-03.png" class="lazyload" alt="produto-03">
                                                <span class="see-product">Ver Produto</span>
                                                <span class="discount">50% <span>off</span></span>
                                            </div>
                                        </a>
                                        <div class="box">
                                            <a href="product.php" class="name">Produto 03</a>
                                            <span class="subtitle">Desc. 03</span>
                                            <div class="price-box">
                                                <span class="price">R$99,90</span>
                                                <span class="installments">
                                                    ou R$110,00
                                                    <br>
                                                    em 10x de R$11,00 com juros
                                                </span>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="item">
                                        <a href="product.php">
                                            <div class="img-item">
                                                <img data-src="imgs/products/product-04.png" class="lazyload" alt="produto-04">
                                                <span class="see-product">Ver Produto</span>
                                            </div>
                                        </a>
                                        <div class="box">
                                            <a href="product.php" class="name">Produto 04</a>
                                            <span class="subtitle">Desc. 04</span>
                                            <div class="price-box">
                                                <span class="price">R$99,90</span>
                                                <span class="installments">
                                                    ou R$110,00
                                                    <br>
                                                    em 10x de R$11,00 com juros
                                                </span>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="item">
                                        <a href="product.php">
                                            <div class="img-item">
                                                <img data-src="imgs/products/product-01.png" class="lazyload" alt="produto-01">
                                                <span class="see-product">Ver Produto</span>
                                            </div>
                                        </a>
                                        <div class="box">
                                            <a href="product.php" class="name">Produto 01</a>
                                            <span class="subtitle">Desc. 01</span>
                                            <div class="price-box">
                                                <span class="price">R$99,90</span>
                                                <span class="installments">
                                                    ou R$110,00
                                                    <br>
                                                    em 10x de R$11,00 com juros
                                                </span>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="item">
                                        <a href="product.php">
                                            <div class="img-item">
                                                <img data-src="imgs/products/product-02.png" class="lazyload" alt="produto-02">
                                                <span class="see-product">Ver Produto</span>
                                                <span class="new">Novo</span>
                                            </div>
                                        </a>
                                        <div class="box">
                                            <a href="product.php" class="name">Produto 02</a>
                                            <span class="subtitle">Desc. 02</span>
                                            <div class="price-box">
                                                <span class="price">R$99,90</span>
                                                <span class="installments">
                                                    ou R$110,00
                                                    <br>
                                                    em 10x de R$11,00 com juros
                                                </span>
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <nav class="pagination-numeric">
                            <a class="prev page-numbers" href="#"><i class="fas fa-angle-left"></i></a>
                            <a class="page-numbers" href="#">1</a>
                            <span class="page-numbers dots">…</span>
                            <a class="page-numbers" href="#">3</a>
                            <span class="page-numbers current">4</span>
                            <a class="page-numbers" href="#">5</a>
                            <span class="page-numbers dots">…</span>
                            <a class="page-numbers" href="#">49</a>
                            <a class="next page-numbers" href="#"><i class="fas fa-angle-right"></i></a>      
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php include('footer.php'); ?>