<?php include ('header.php'); ?>

	<section class="page-404">
		<div class="container">
			<span><strong>404</strong></span>
			<p><strong>Página não encontrada</strong></p>
			<a href="<?php //bloginfo('url')?> index.php">Voltar para a página inicial <i class="fas fa-undo"></i></a>
		</div>
	</section>

<?php include('footer.php'); ?>