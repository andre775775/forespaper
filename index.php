<?php include('header.php'); ?>

<div id="home">
    <h1 class="hidden-text">Forest Paper Clone</h1>
    <section class="banner banner-home slider-d-none">
        <div class="item">
            <a href="category.php">
                <picture>
                    <source media="(max-width: 767px)" srcset="../forest paper/imgs/banners/banner-1-m.png">
                    <img src="../forest paper/imgs/banners/banner-1.png" class="img-cover" alt="">
                </picture>
            </a>
        </div>

    </section>
    <section class="stripe">
        <div class="container">
            <div class="slider-stripe slider-d-none">
                <div class="item">
                    <div class="item--img">
                        <img data-lazy="imgs/icons/stripe/icon-3.png" alt="">
                    </div>
                    <div class="item--desc">
                        <p>Site <strong>100% Seguro</strong></p>
                    </div>
                </div>
                <div class="item">
                    <div class="item--img">
                        <img data-lazy="imgs/icons/stripe/icon-4.png" alt="">
                    </div>
                    <div class="item--desc">
                        <p>1ª Troca Gratuita</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="mini-banners">
        <div class="container">
            <div class="banner slider-mini-banners slider-d-none">
                <div class="item">
                    <a href="#">
                        <picture>
                            <img data-lazy="imgs/banners/mini-banner.png" class="img-cover" alt="">
                        </picture>
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <picture>
                            <img data-lazy="imgs/banners/mini-banner.png" class="img-cover" alt="">
                        </picture>
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <picture>
                            <img data-lazy="imgs/banners/mini-banner.png" class="img-cover" alt="">
                        </picture>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="list-items">
        <div class="container">
            <h2 class="title color-2 text-center">Recomendados <i class="fa-solid fa-rss"></i></h2>
            <div class="row row-cols-2 row-cols-md-3 row-cols-lg-4">
                <div class="col">
                    <div class="item">
                        <a href="product.php">
                            <div class="img-item">
                                <img class="lazyload" data-src="imgs/products/product-01.png" alt="produto-01">
                                <span class="see-product">Ver Produto</span>
                            </div>
                        </a>
                        <div class="box">
                            <a href="product.php" class="name">Produto 01</a>
                            <span class="subtitle">Desc. 01</span>
                            <div class="price-box">
                                <span class="old-price"><del>de R$120,00</del></span>
                                <span class="price">R$99,90</span>
                                <span class="installments">
                                    ou R$110,00
                                    <br>
                                    em 10x de R$11,00 com juros
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="item">
                        <a href="product.php">
                            <div class="img-item">
                                <img class="lazyload" data-src="imgs/products/product-02.png" alt="produto-02">
                                <span class="see-product">Ver Produto</span>
                                <span class="new">Novo</span>
                            </div>
                        </a>
                        <div class="box">
                            <a href="product.php" class="name">Produto 02</a>
                            <span class="subtitle">Desc. 02</span>
                            <div class="price-box">
                                <span class="price">R$99,90</span>
                                <span class="installments">
                                    ou R$110,00
                                    <br>
                                    em 10x de R$11,00 com juros
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="item">
                        <a href="product.php">
                            <div class="img-item">
                                <img class="lazyload" data-src="imgs/products/product-03.png" alt="produto-03">
                                <span class="see-product">Ver Produto</span>
                                <span class="discount">50% <span>off</span></span>
                            </div>
                        </a>
                        <div class="box">
                            <a href="product.php" class="name">Produto 03</a>
                            <span class="subtitle">Desc. 03</span>
                            <div class="price-box">
                                <span class="old-price"><del>de R$120,00</del></span>
                                <span class="price">R$99,90</span>
                                <span class="installments">
                                    ou R$110,00
                                    <br>
                                    em 10x de R$11,00 com juros
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="item">
                        <a href="product.php">
                            <div class="img-item">
                                <img class="lazyload" data-src="imgs/products/product-04.png" alt="produto-04">
                                <span class="see-product">Ver Produto</span>
                            </div>
                        </a>
                        <div class="box">
                            <a href="product.php" class="name">Produto 04</a>
                            <span class="subtitle">Desc. 04</span>
                            <div class="price-box">
                                <span class="price">R$99,90</span>
                                <span class="installments">
                                    ou R$110,00
                                    <br>
                                    em 10x de R$11,00 com juros
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="categories">
        <div class="container">
            <h1 class="title text-center"><strong>Navegue pelas Marcas</strong></h1>
            <div class="slider-categories">
                <div class="item">
                    <a href="category.php">
                        <img data-lazy="imgs/icons/category/category-4.png">
                    </a>
                </div>
                <div class="item">
                    <a href="category.php">
                        <img data-lazy="imgs/icons/category/category-4.png">
                    </a>
                </div>
                <div class="item">
                    <a href="category.php">
                        <img data-lazy="imgs/icons/category/category-4.png">
                    </a>
                </div>
                <div class="item">
                    <a href="category.php">
                        <img data-lazy="imgs/icons/category/category-4.png">
                    </a>
                </div>
                <div class="item">
                    <a href="category.php">
                        <img data-lazy="imgs/icons/category/category-4.png">
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="newsletter">
        <div class="container" >
            <div class="col-md-5 col-lg-7" id="new">
                <h1><strong>Receba nossas ofertas exclusivas!</strong></h1>
                <p>Receba ofertas, descontos e informações exclusivas.</p>
                <input class="" type="text" name="email" id="email" placeholder="E-mail">
                <span><button class="btn btn-secondary"><strong>Assinar</strong></button></span>
            </div>
        </div>
    </section>
    <section class="mini-banners">
        <div class="container">
            <div class="banner slider-mini-banners slider-d-none">
                <div class="item">
                    <a href="#">
                        <picture>
                            <img data-lazy="imgs/banners/mini-banner.png" class="img-cover" alt="">
                        </picture>
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <picture>
                            <img data-lazy="imgs/banners/mini-banner.png" class="img-cover" alt="">
                        </picture>
                    </a>
                </div>
                <div class="item">
                    <a href="#">
                        <picture>
                            <img data-lazy="imgs/banners/mini-banner.png" class="img-cover" alt="">
                        </picture>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="" id="instagram">
            <div class="container" >
                  
               <div id="vermais" class="">
               <h1 class=""><strong>Confira nosso Instagram</strong></h1> 
                <span><a href="https://www.instagram.com/" title="Instagram" target="_blank" rel="noreferrer">
                    <i class="fab fa-instagram "></i></a> Ver Mais</span>
            </div>
            <div class="slider-categories">
                <div class="item">
                    <a href="https://www.instagram.com/" title="Instagram" target="_blank" rel="noreferrer">
                        <img data-lazy="imgs/insta.png">
                    </a>
                </div>
                <div class="item">
                    <a href="category.php">
                        <img data-lazy="imgs/insta.png">
                    </a>
                </div>
                <div class="item">
                    <a href="category.php">
                        <img data-lazy="imgs/insta.png">
                    </a>
                </div>
                <div class="item">
                    <a href="category.php">
                        <img data-lazy="imgs/insta.png">
                    </a>
                </div>
                <div class="item">
                    <a href="category.php">
                        <img data-lazy="imgs/insta.png">
                    </a>
                </div>
                <div class="item">
                    <a href="category.php">
                        <img data-lazy="imgs/insta.png">
                    </a>
                </div>
                <div class="item">
                    <a href="category.php">
                        <img data-lazy="imgs/insta.png">
                    </a>
                </div>
                <div class="item">
                    <a href="category.php">
                        <img data-lazy="imgs/insta.png">
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- <section class="instagram">
        <div class="container">
            <h2><strong>Confira Nosso Instagram</strong></h2>
            <div id="vermais">
                <p><a href="https://www.instagram.com/" title="Instagram" target="_blank" rel="noreferrer">
                    <i class="fab fa-instagram"></i></a> Ver Mais</p>
            </div>
        </div>
        <div class="container">
            <img src="imgs/insta.png" alt="" srcset="">
            <img src="imgs/insta.png" alt="" srcset="">
            <img src="imgs/insta.png" alt="" srcset="">
            <img src="imgs/insta.png" alt="" srcset="">
            <img src="imgs/insta.png" alt="" srcset="">
            <img src="imgs/insta.png" alt="" srcset="">
            <img src="imgs/insta.png" alt="" srcset="">
        </div>
       
    </section> -->
    <div class="container" id="footerbanner">
         <picture>
                <source media="(max-width: 767px)" srcset="imgs/footerbannermini.png">
                    <img src="imgs/footerbanner.png"  alt="">
                </picture>
         </div>

    <?php include('footer.php'); ?>