<?php include('header.php'); ?>

    <div id="login" class="login-layout">
        <section class="container container-small">
            <div class="box">
                <h1 class="title color-2 text-center">Faça agora seu login</h1>
                <hr/>
                <form action="" method="POST" class="form-layout">
                    <!--
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        ERRO! Login ou Senha incorretos.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    -->
                    <div class="form-group">
                        <label for="form-login">Login</label>
                        <input name="form-login" id="form-login" type="text" required/>
                    </div>
                    <div class="form-group">
                        <label for="form-password">Senha</label>
                        <input name="form-password" id="form-password" type="password" required>
                        <span><a href="forgot-password.php" class="link">Esqueci minha senha</a></span>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit">Fazer Login</button>
                        <span>Ou faça seu cadastro <a href="signup.php" class="link">clicando aqui</a></span>
                    </div>
                </form>
            </div>
        </section>
        <section class="social-buttons">
            <div class="container">
                <div class="row row-cols-1">
                    <div class="col">
                        <div class="btn-social">
                            <a class="face" href="#" target="_blank" rel="noreferrer">
                                <i class="fab fa-facebook-square"></i>
                                <span>Login com Facebook</span>
                            </a>
                        </div>
                    </div>
                    <div class="col">
                        <div class="btn-social">
                            <a class="google" href="#" target="_blank" rel="noreferrer">
                                <i class="fab fa-google"></i>
                                <span>Login com Google</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

<?php include('footer.php'); ?>