<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Site, Base">
    <title>Base Ecommerce Essencial</title>
    <link rel="apple-touch-icon" sizes="57x57" href="imgs/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="imgs/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="imgs/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="imgs/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="imgs/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="imgs/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="imgs/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="imgs/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="imgs/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="imgs/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="imgs/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="imgs/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="imgs/favicon/favicon-16x16.png">
    <link rel="manifest" href="imgs/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="imgs/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel ="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"/>
    <link rel="stylesheet" href="libs/fontawesome-free-5.13.1/css/all.min.css" />
    <link rel="stylesheet" href="libs/fancybox-3.5.7/dist/jquery.fancybox.min.css" />
    <link rel="stylesheet" href="libs/slick-slider-1.8.1/slick/slick.css" />
    <link rel="stylesheet" href="libs/slick-slider-1.8.1/slick/slick-theme.css" />
    <link rel="stylesheet" href="libs/btn-whatsapp/dist/css/style.css" />
    <link rel="stylesheet" href="libs/simplebar/simplebar.css">
    <?php if (strpos($_SERVER['REQUEST_URI'], 'product.php')) : ?>
        <link rel="stylesheet" href="libs/btn-installments/css/installments.css" />
    <?php endif; ?>
    <link rel="stylesheet" href="css/style.css" />
</head>

<body>
    <header id="header">
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-4">
                        <a href="https://api.whatsapp.com/send?phone=5542999999999"><i class="fab fa-whatsapp"></i>55 (11) 99968 3766</a>
                    </div>
                    
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand compensate-for-scrollbar">
            <div class="container">
                <div class="logo">
                    <a href="index.php">
                        <img srcset="imgs/logo.png, imgs/logo-2x.png 2x" src="imgs/logo-2x.png" alt="Base Ecommerce Essencial" />
                    </a>
                </div>
                <div class="search-box">
                    <form action="search.php" method="GET">
                        <input type="text" name="search" autocomplete="off" placeholder="Digite aqui o que você procura" />
                        <button type="submit"><i class="fas fa-search"></i><span class="hidden-text">Pesquisar</span></button>
                    </form>
                </div>
                <div class="div">
                <ul class="user-buttons">
                    <li>
                        <a href="#" title="Lista de Desejos">
                            <i class="fas fa-heart"></i>
                            <p>Lista de <br />Desejos</p>
                        </a>
                    </li>
                    <li>
                        <a href="cart.php" title="Carrinho">
                            <span class="cart-count">4</span>
                            <i class="fas fa-shopping-cart"></i>
                            <p>Carrinho</p>
                        </a>
                    </li>
                    <li>
                        <a href="signup.php" title="User">
                            <i class="fa-solid fa-user"></i>
                            <p>Perfil</p>
                        </a>
                    </li>
                </ul>
                </div>
            </div>
        </nav>
        <div class="nav">
            <div class="container">
                <ul class="navbar-nav">
                    <li class="custom-dropdown"><a href="#"><i class="fas fa-bars"></i> Todas as Categorias</a></li>
                    <li><a href="category.php">Categoria 1</a></li>
                    <li><a href="category.php">Categoria 2</a></li>
                    <li><a href="category.php">Categoria 3</a></li>
                    <li><a href="category.php">Categoria 4</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categoria 5 <i class="fas fa-angle-down"></i></a>
                        <div class="dropdown-menu">
                            <a href="category.php">Item 1</a>
                            <a href="category.php">Item 2</a>
                            <a href="category.php">Item Lorem Ipsum Dolor</a>
                        </div>
                    </li>
                </ul>
                <div class="menu-full">
                    <div class="menu-content">
                        <ul class="nav nav-pills" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <span class="tit">Todas as Categorias</span>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" id="menu-item-tab-1" data-toggle="pill" href="#menu-item-1" role="tab" aria-controls="menu-item-1" aria-selected="true">Categoria 1</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="menu-item-tab-2" data-toggle="pill" href="#menu-item-2" role="tab" aria-controls="menu-item-2" aria-selected="false">Categoria 2</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="menu-item-tab-3" data-toggle="pill" href="#menu-item-3" role="tab" aria-controls="menu-item-3" aria-selected="false">Categoria 3 Lorem ipsum dolor sit</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="menu-item-tab-4" data-toggle="pill" href="#menu-item-4" role="tab" aria-controls="menu-item-4" aria-selected="false">Categoria 4</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="menu-item-1" role="tabpanel" aria-labelledby="menu-item-tab-1">
                                <ul>
                                    <li><a href="category.php">Categoria 1</a></li>
                                    <li><a href="category.php">Categoria 2</a></li>
                                    <li><a href="category.php">Categoria 3</a></li>
                                    <li><a href="category.php">Categoria 4</a></li>
                                    <li><a href="category.php" class="see-more">Ver Todas</a></li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="menu-item-2" role="tabpanel" aria-labelledby="menu-item-tab-2">
                                <ul>
                                    <li><a href="category.php">Categoria 2 1</a></li>
                                    <li><a href="category.php">Categoria 2 2</a></li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="menu-item-3" role="tabpanel" aria-labelledby="menu-item-tab-3">
                                <ul>
                                    <li><a href="category.php">Categoria 3 1</a></li>
                                    <li><a href="category.php">Categoria 3 2</a></li>
                                    <li><a href="category.php">Categoria 3 3</a></li>
                                    <li><a href="category.php">Categoria 3 4</a></li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="menu-item-4" role="tabpanel" aria-labelledby="menu-item-tab-4">
                                <ul>
                                    <li><a href="category.php">Categoria 4 1</a></li>
                                    <li><a href="category.php">Categoria 4 2</a></li>
                                    <li><a href="category.php">Categoria 4 3</a></li>
                                    <li><a href="category.php">Categoria 4 4</a></li>
                                    <li><a href="category.php">Categoria 4 5</a></li>
                                    <li><a href="category.php">Categoria 4 6</a></li>
                                    <li><a href="category.php">Categoria 4 7</a></li>
                                    <li><a href="category.php">Categoria 4 8</a></li>
                                    <li><a href="category.php">Categoria 4 9</a></li>
                                    <li><a href="category.php">Categoria 4 10</a></li>
                                    <li><a href="category.php">Categoria 4 11</a></li>
                                    <li><a href="category.php">Categoria 4 12</a></li>
                                    <li><a href="category.php">Categoria 4 13</a></li>
                                    <li><a href="category.php">Categoria 4 14</a></li>
                                    <li><a href="category.php">Categoria 4 15</a></li>
                                    <li><a href="category.php">Categoria 4 16</a></li>
                                    <li><a href="category.php">Categoria 4 17</a></li>
                                    <li><a href="category.php">Categoria 4 18</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="search-mobile d-lg-none">
        <form action="search.php" method="GET">
            <input type="text" name="search" autocomplete="off" placeholder="Digite aqui o que você procura" />
            <button type="submit"><i class="fas fa-search"></i></button>
        </form>
    </div>
    <div id="wrapper">
        <div class="overlay"></div>
        <i class="close-menu-mobile fas fa-times"></i>
            <div id="sidebar-menu">

                <ul class="sidebar-nav" id="submenu-mobile">
                    <li><a href="category.php">Categoria 1</a></li>
                    <li><a href="category.php">Categoria 2</a></li>
                    <li><a href="category.php">Categoria 3</a></li>
                    <li><a href="category.php">Categoria 4</a></li>
                    <li><a href="category.php">Categoria 5</a></li>
                    <li>
                        <a href="#sub-1" data-toggle="collapse" class="btn-collapse collapsed">Todas as Categorias <i class="caret fas fa-angle-up"></i></a>
                        <div class="collapse" id="sub-1">
                            <div class="sub-menu">
                                <a href="category.php">Categoria 1</a>
                                <a href="category.php">Categoria 2</a>
                                <a href="category.php">Categoria 3</a>
                                <a href="category.php">Categoria 4</a>
                                <a href="category.php">Categoria 5</a>
                            </div>
                        </div>
                    </li>
                    <li class="social">
                        <a href="https://facebook.com/" title="Facebook" target="_blank" rel="noreferrer"><i class="fab fa-facebook-square"></i></a>
                        <a href="https://twitter.com" title="Twitter" target="_blank" rel="noreferrer"><i class="fab fa-twitter"></i></a>
                        <a href="https://www.instagram.com/" title="Instagram" target="_blank" rel="noreferrer"><i class="fab fa-instagram"></i></a>
                        <!--
                        <a href="https://api.whatsapp.com/send?phone=5542999999999" title="Whatsapp" target="_blank" rel="noreferrer"><i class="fab fa-whatsapp"></i></a>
                        -->
                    </li>
                </ul>
            </div>
            <div id="mobile-bar">
                <div class="container">
                    <ul>
                        <li>
                            <a href="index.php">
                                <i class="fas fa-home"></i>
                                <p>Início</p>
                            </a>
                        </li>
                        <li>
                            <button id="menu-toggle">
                                <i class="fas fa-bars"></i>
                                <p>Categorias</p>
                            </button>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fas fa-heart"></i>
                                <p>Favoritos</p>
                            </a>
                        </li>
                        <li>
                            <a href="cart.php" title="Carrinho">
                                <span class="cart-count">4</span>
                                <i class="fas fa-shopping-cart"></i>
                                <p>Carrinho</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fas fa-user"></i>
                                <p>Minha Conta</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>