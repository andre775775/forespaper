<?php include('header.php'); ?>

    <div id="forgot-password" class="login-layout">
        <section class="container">
            <div class="box">
                <h1 class="title color-2 text-center">Esqueci minha senha</h1>
                <div class="page">
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                </div>
                <form action="recovery-password.php" method="POST" class="form-layout">
                    <!--
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        ERRO! E-mail não cadastrado.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    -->
                    <div class="form-group">
                        <label for="form-email">Seu e-mail</label>
                        <input name="form-email" id="form-email" type="email" required/>
                    </div>

                    <div class="form-group form-buttons">
                        <div class="recaptcha">
                            <div class="g-recaptcha" data-theme="light" data-sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"></div>
                        </div>
                        <button type="submit">ENVIAR</button>
                    </div>
                </form>
            </div>
        </section>
    </div>

<?php include('footer.php'); ?>