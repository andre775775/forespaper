jQuery(document).ready(function(){

//--Tooltip---------------------------------------------------------------------------------------//

    jQuery('[data-toggle="tooltip"]').tooltip();

//--Change product image height in mobile---------------------------------------------------------//

    jQuery(window).resize(function() {
        setImageHeight();
    });

//--Footer Aways Bottom---------------------------------------------------------------------------//

    jQuery(window).resize(function() {
        var pageHeight = jQuery(window).outerHeight() - (jQuery('footer').outerHeight() + jQuery('header').outerHeight());
        jQuery('#wrapper').css('min-height', pageHeight+'px');
    });
    
    jQuery(window).trigger('resize');

//--Filter Collapse-------------------------------------------------------------------------------//

    if (window.matchMedia('(min-width: 992px)').matches) {
        jQuery('#filters').addClass('show');
    }

//--Toggle Grid List Items------------------------------------------------------------------------//

    jQuery('.list-order .sort-type').click(function() {
        jQuery('.list-order .sort-type').removeClass('active');
        jQuery(this).addClass('active');
        if(jQuery(this).is('#list-item-grid')) {
            jQuery('.list-items #list-box').removeAttr('class');
            jQuery('.list-items #list-box').attr('class', 'row row-cols-1 row-cols-sm-2 row-cols-md-3');
        } else {
            jQuery('.list-items #list-box').removeAttr('class');
            jQuery('.list-items #list-box').attr('class', 'line-list row row-cols-1');
        }
    });

//--Slider Home-----------------------------------------------------------------------------------//

    jQuery('.banner-home').slick({
        slidesToShow                : 1,
        autoplay                    : true,
        autoplaySpeed               : 5000,
        pauseOnHover                : false,
        //fade                        : true,
    });

//--Mini Banners----------------------------------------------------------------------------------//

    jQuery('.slider-mini-banners').slick({
        slidesToShow                : 3,
        autoplay                    : true,
        autoplaySpeed               : 4000,
        pauseOnHover                : false,
        responsive                  : [
            {
                breakpoint          : 992,
                settings            : {
                    slidesToShow    : 2
                }
            },
            {
                breakpoint          : 576,
                settings            : {
                    slidesToShow    : 1
                }
            }
        ]
    });

//--Slider Stripe---------------------------------------------------------------------------------//

    jQuery('.slider-stripe').slick({
        slidesToShow                : 4,
        infinite                    : false,
        responsive                  : [
            {
                breakpoint          : 1200,
                settings            : {
                    slidesToShow    : 3
                }
            },
            {
                breakpoint          : 992,
                settings            : {
                    slidesToShow    : 2
                }
            },
            {
                breakpoint          : 576,
                settings            : {
                    slidesToShow    : 1
                }
            }
        ]
    });

//--Slider Categories-----------------------------------------------------------------------------//

    jQuery('.slider-categories').slick({
        slidesToShow                : 5,
        infinite                    : false,
        responsive                  : [
            {
                breakpoint          : 992,
                settings            : {
                    slidesToShow: 4
                }
            },
            {
                breakpoint          : 768,
                settings            : {
                    slidesToShow: 3
                }
            },
            {
                breakpoint          : 576,
                settings            : {
                    slidesToShow: 2
                }
            },
            {
                breakpoint          :360,
                settings            : {
                    slidesToShow: 1
                }
            }
        ]
    });


//--Slider Contact--------------------------------------------------------------------------------// 

    jQuery('.slider-address').slick({
        slidesToShow                : 1,
        prevArrow                   : jQuery('.btn-prev'),
        nextArrow                   : jQuery('.btn-next')
    });

//--Slider Product Single / Zoom------------------------------------------------------------------// 

    jQuery('.slider-product-img').slick({
        slidesToShow                : 1,
        slidesToScroll              : 1,
        arrows                      : false,
        fade                        : true,
        infinite                    : false,
        swipe                       : false,
        asNavFor                    : '.slider-product-thumb',
        responsive                  : [
            {
                breakpoint          : 1200,
                settings            : {
                    fade            : false,
                    swipe           : true
                }
            }
        ]
    });

    jQuery('.slider-product-thumb').slick({
        slidesToShow                : 5,
        slidesToScroll              : 1,
        arrows                      : true,
        dots                        : false,
        focusOnSelect               : true,
        vertical                    : true,
        verticalSwiping             : false,
        infinite                    : false,
        asNavFor                    : '.slider-product-img',
        responsive                  : [
            {
                breakpoint          : 1200,
                settings            : {
                    vertical        : false,
                }
            },
            {
                breakpoint          : 575,
                settings            : {
                    slidesToShow    : 4,
                    vertical        : false,
                }
            }
        ]
    });

    //Zoom
    var img_zoom = jQuery('#img-zoom-0');
    var window_size = jQuery(window).width();
    if(window_size > 1200){
        img_zoom.elevateZoom({
            zoomWindowFadeIn    : 400,
            zoomWindowFadeOut   : 400,
            lensFadeIn          : 400,
            lensFadeOut         : 400,
            cursor              : 'crosshair',
            zoomType            : 'inner'
        }); 
    } 

    // Destroy and Re-create the Zoom
    jQuery('.slider-product-img').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        jQuery('.zoomContainer').remove();
            var img_zoom = jQuery('#img-zoom-' + nextSlide);
            var window_size = jQuery(window).width();
            if(window_size > 1200){
                img_zoom.elevateZoom({
                zoomWindowFadeIn    : 400,
                zoomWindowFadeOut   : 400,
                lensFadeIn          : 400,
                lensFadeOut         : 400,
                cursor              : 'crosshair',
                zoomType            : 'inner'
            }); 
        }
    });

//--Mask------------------------------------------------------------------------------------------//

    jQuery('.cep-mask').mask('00000-000');
    jQuery('.cnpj-mask').mask('000.000.000/0000-00');
    jQuery('.cpf-mask').mask('000.000.000-00');
    jQuery('.date-mask').mask('00/00/0000', {
        placeholder: "00/00/0000"}
    );
    jQuery('.insc-mask').mask('00.000.000-0 - SS');
    jQuery('.num-mask').mask('0000');
    jQuery('.tel-mask, .cel-mask').mask(SPMaskBehavior, spOptions);

//--Fancybox 3------------------------------------------------------------------------------------//

    jQuery('[data-fancybox]').fancybox({
        animationEffect: 'fade'
    });

//--Custom Select---------------------------------------------------------------------------------//

    jQuery('.select-custom').each(function () {
        if (jQuery(this).hasClass('validation-failed')) {
            jQuery(this).closest('.select-custom-box').addClass('validation-failed')
        }
        var selectLabel = jQuery(this).find(":selected").text();
        var label = jQuery(this).attr('label');
        var first = jQuery(this).val();
        if(!first) {
            jQuery('#'+label).html(`<span>${selectLabel}</span>`);
            return
        }
        jQuery('#' + label).text(selectLabel);
    });

//--Change Colors----------------------------------------------------------------------------------//

    jQuery('.box-check.colors .check-radio').click(function() {
        if(jQuery(this).is(':checked')) { 
            var labelColor = jQuery(this).next().html();
            jQuery('.colors .color-txt').html(labelColor);
        }
    });

//--Custom Dropdown-------------------------------------------------------------------------------//

    jQuery('header .nav .dropdown').on('show.bs.dropdown', function (e) {
        preventDefault(e);
    });

    jQuery('header .nav .dropdown').on('mouseenter', function(e) {
        jQuery('.overlay').addClass('active');
        jQuery('.menu-full').removeClass('show');
        jQuery('.custom-dropdown a').removeClass('active');
    });

    jQuery('header .nav .dropdown').on('mouseleave', function(e) {
        jQuery('.overlay').removeClass('active');
    });

    jQuery('.custom-dropdown a').on('click', function(e) {
        e.preventDefault();
        jQuery('.menu-full').toggleClass('show');
        jQuery('.overlay').toggleClass('active');
        jQuery(this).toggleClass('active');
    });

//--Toggle Menu Mobile----------------------------------------------------------------------------//

    jQuery('#menu-toggle').click(function(e) {
        e.preventDefault();
        jQuery('#sidebar-menu').toggleClass('toggled');
        jQuery('.overlay').toggleClass('active');
        jQuery('body').toggleClass('toggled');
        jQuery('.close-menu-mobile').addClass('show');
    });

    jQuery('.overlay, .close-menu-mobile').click(function(e) {
        e.preventDefault();
        jQuery('#sidebar-menu').removeClass('toggled');
        jQuery('.overlay').removeClass('active');
        jQuery('body').removeClass('toggled');
        jQuery('.close-menu-mobile').removeClass('show');
        jQuery('.menu-full').removeClass('show');
        jQuery('.custom-dropdown a').removeClass('active');
    });

//--Fix Modal Bootstrap bug header fixed----------------------------------------------------------//

    var nb = jQuery('header');
    jQuery('.modal')
        .on('show.bs.modal', function () {
            nb.width(nb.width());
        })
        .on('hidden.bs.modal', function () {
            nb.width(nb.width('auto'));
        });

//--Botão de Scroll Top---------------------------------------------------------------------------//

    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() >= 600) {
            jQuery('.scroll-top').addClass('active');
        } else {
            jQuery('.scroll-top').removeClass('active');
        }
    });

    jQuery('.scroll-top').click(function() {
        jQuery('body,html').animate({ scrollTop : 0 }, 500);
    });

//--Collapse--------------------------------------------------------------------------------------//

    jQuery('.btn-collapse').click(function() {
        jQuery(' .collapse, .collapsed').collapse('hide');
    });

//--Cart Quantity----------------------------------------------------------------------------------//

    jQuery('<button class="number__btn number__btn--down"></button>').insertBefore('.number__field');
    jQuery('<button class="number__btn number__btn--up"></button>').insertAfter('.number__field');

    jQuery('.number').each(function() {
        var spinner = jQuery(this),
            input = spinner.find('.number__field'),
            btnUp = spinner.find('.number__btn.number__btn--up'),
            btnDown = spinner.find('.number__btn.number__btn--down'),
            min = input.attr('min'),
            max = input.attr('max'),
            step = input.attr('step'); 
        btnUp.click(function(e) {
            e.preventDefault();
            var oldValue = parseFloat(input.val());
            if (oldValue >= max) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue + parseFloat(step);
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

        btnDown.click(function(e) {
            e.preventDefault();
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue - parseFloat(step);
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });
    });

}); // END DOCUMENT.READY

//--Altenate form between person and legal-----------------------------------------------//

jQuery('input[name=person]').on('change', function() {
    let type = jQuery('input[name=person]:checked').val();
    if (type == 1) {
        jQuery('.physical-person').fadeIn(400);
        jQuery('.legal-person').css('display', 'none');
    } else if(type == 2) {
        jQuery('.legal-person').fadeIn(400);
        jQuery('.physical-person').css('display', 'none');
    }
});

//--Estado-------------------------------------------------------------------------------//

jQuery.getJSON('libs/json-states/states-cities.json', function (data) {
    var items = [];
    var options = '<option value=""></option>';

    jQuery.each(data, function (key, val) {
        options += '<option value="' + val.sigla + '">' + val.sigla + '</option>';
    });

    jQuery(".estado").html(options);

    jQuery(".estado").change(function () {
        var str = "";

        jQuery(this).find("option:selected").each(function () {
            str += jQuery(this).text();
        });
    }).change();

});

//--Mask for 9° digit-----------------------------------------------------------------------------//

var SPMaskBehavior = function (val) {
  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
spOptions = {
  onKeyPress: function(val, e, field, options) {
      field.mask(SPMaskBehavior.apply({}, arguments), options);
    }
};

//--Change product image height in mobile---------------------------------------------------------//

function setImageHeight() {
    let image = jQuery('.list-items .item .img-item');
    let imageWidth = image.outerWidth();

    let imageSingle = jQuery('.slider-product-img');
    let imageWidthSingle = imageSingle.outerWidth();

    if (window.matchMedia('(max-width: 576px)').matches) {
        image.css('min-height' ,imageWidth);
        imageSingle.find('.item').css('min-height' ,imageWidthSingle);
        return
    }

    image.css('min-height', 'auto');
    imageSingle.find('.item').css('min-height' ,'auto');
}

//--Copy to Clipboard-----------------------------------------------------------------------------//

var copyText = document.getElementById("copy-to-clipboard");

function copyToClipboard () {
    /* Get the text field */
  
    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /* For mobile devices */
  
    /* Copy the text inside the text field */
    document.execCommand("copy");

    jQuery('#copy-to-clipboard').tooltip('show')
    setTimeout(function() {
        jQuery('#copy-to-clipboard').tooltip('hide')
    }, 3000)
}

//--Text Area Ajustable Height--------------------------------------------------------------------//

if(jQuery('#copy-to-clipboard')[0]) {
    function auto_grow(element) {
        element.style.height = "5px";
        element.style.height = (element.scrollHeight + 30)+"px";
    }
    
    window.onload = auto_grow(copyText)
}

//--Adjust Scroll Height--------------------------------------------------------------------------//

if(jQuery('.resume .list-prod')[0]) {
    if (window.matchMedia('(min-width: 767px)').matches) {
        let height = jQuery('#table').outerHeight()
        jQuery('.resume .list-prod').css('height', height)
    }
}

//--Sidebar Filter Scrollbar----------------------------------------------------------------------//

let listFilter = document.querySelectorAll('#sidebar-filter .filter .list-filter-items');
listFilter.forEach(function(e) {
    let totalItems =  e.querySelectorAll('li').length;
    if(totalItems > 6 && !e.closest('.sizes')) {
        return new SimpleBar(e);
    }
    if(totalItems > 20 && e.closest('.sizes')) {
        return new SimpleBar(e);
    }
});

//--Exempt BTN----------------------------------------------------------------------//
function exemptBtn(){
    let insc = document.getElementById("insc");

    if(insc.disabled === true){
        insc.disabled = false;
        document.getElementById("exempt-icon").classList.remove("active")
    }
    else{
        insc.disabled = true;
        insc.value = null;
        document.getElementById("exempt-icon").classList.add("active")
    }
}

//--Custom Select---------------------------------------------------------------------------------//

jQuery(document).on('change', '.select-custom-box .select-custom', function () {
    var defaulttext2 = jQuery(this).find(":selected").text(); 
    var label = jQuery(this).attr('label');
    var first = jQuery(this).val();
    if(!first) {
        jQuery('#'+label).html(`<span>${defaulttext2}</span>`);
        return
    }
    jQuery('#'+label).text(defaulttext2);
})

//--Outer Height----------------------------------------------------------------------------------//

function outerHeight(el) { // Calculate the outer height (el + padding + margin) of an element
    const curStyle = el.currentStyle || window.getComputedStyle(el);
    let outerHeight = el.offsetHeight;
    outerHeight += parseInt(curStyle.marginTop);
    outerHeight += parseInt(curStyle.marginBottom);
    return outerHeight;
}

//--Menu Scroll Fixed-----------------------------------------------------------------------------//

let navHeight = outerHeight(document.getElementById('header'));
let searchHeight  = outerHeight(document.querySelector(".search-mobile")) 
function menuFixedOnScroll(screenSize) {
    const footer = document.getElementById('footer'),
        mobileBar = document.getElementById('mobile-bar'),
        nav = document.getElementById('header'),
        wrapper = document.getElementById('wrapper');
    if (screenSize.matches) {
        mobileBar.classList.remove("active");
        footer.style.marginBottom = 0;
        document.addEventListener("scroll", function (event) {
            if (screenSize.matches) {
                let windowTop = document.documentElement.scrollTop;
                if (windowTop > navHeight) {
                    nav.classList.add('nav-small');
                    wrapper.style.paddingTop = navHeight + 'px';
                } else {
                    nav.classList.remove('nav-small');
                    wrapper.style.paddingTop = 0;
                }
            }
        });
    }
    else {
        nav.classList.remove('nav-small');
        mobileBar.classList.add("active");
        footer.style.marginBottom = mobileBar.offsetHeight + "px";

        document.addEventListener("scroll", function (event) {
                let windowTop = document.documentElement.scrollTop;
                if (windowTop >  (searchHeight + navHeight)) {
                    document.querySelector(".search-mobile").classList.add("active");
                    wrapper.style.paddingTop = searchHeight + "px";
                } else {
                    document.querySelector(".search-mobile").classList.remove("active");
                    wrapper.style.paddingTop = 0;
                }
        });
    }
}
var screenSize = window.matchMedia("(min-width: 769px)");
window.addEventListener("DOMContentLoaded", function () {
    menuFixedOnScroll(screenSize)
}, true);
window.addEventListener("resize", function () {
    menuFixedOnScroll(screenSize)
}, true);

