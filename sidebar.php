<aside id="sidebar">
    <ul class="list-unstyled">
        <li>
            <h2 class="subtitle">Ajuda / Central de Relacionamento</h2>
        </li>
        <li>
            <a href="page.php" class="active">Quem Somos</a>
        </li>
        <li>
            <a href="contact.php">Contato / Localização</a>
        </li>
        <li>
            <a href="page.php">Trocas e Devoluções</a>
        </li>
        <li>
            <a href="page.php">Prazos de Entrega</a>
        </li>
        <li>
            <a href="page.php">Políticas e Privacidade</a>
        </li>
        <li>
            <a href="page.php">Formas de Pagamento</a>
        </li>
    </ul>
</aside>