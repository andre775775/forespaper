<?php include('header.php'); ?>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-3">
                    <?php include('sidebar.php'); ?>
                </div>
                <div class="col-md-8 col-lg-9">
                    <div class="content">
                        <h1 class="title bg-full text-uppercase">Contatos/Localização</h1>
                        <div class="page">
                            <p>Confira nossas redes sociais e endereços!</p>
                        </div>
                        <div class="select-custom-box">
                            <span class="select-label" id="select-city">Selecione a Cidade</span>
                            <select name="city" id="city" class="select-custom" label="select-city" required>
                                <option value="">Selecione a Cidade</option>
                                <option value="Curitiba">Curitiba</option>
                                <option value="Guarapuava">Guarapuava</option>
                                <option value="Ponta Grossa">Ponta Grossa</option>
                            </select>
                        </div>
                        <div class="address">
                            <div class="slider-address">
                                <div class="item">
                                    <div class="row row-cols-1 row-cols-sm-2">
                                        <div class="col">
                                            <h2 class="address--name">Filial 1</h2>
                                            <address>
                                                <a href="https://api.whatsapp.com/send?phone=5542999999999" target="_blank" rel="noreferrer"><i class="fab fa-whatsapp"></i>42 9 9999 9999</a>
                                                <a href="tel:4233333333"><i class="fas fa-phone"></i>42 3333 3333</a>
                                                <a href="https://www.instagram.com/" target="_blank" rel="noreferrer"><i class="fab fa-instagram"></i>/user</a>
                                                <a href="https://facebook.com/" target="_blank" rel="noreferrer"><i class="fab fa-facebook-square"></i>/user</a>
                                                <a href="mailto:email@email.com"><i class="fas fa-envelope"></i>email@email.com</a>
                                                <p><i class="fas fa-map-marker-alt"></i>Av. Rua Endereço, 4242</p>
                                                <p>Guarapuava/PR</p>
                                            </address>
                                            <div class="btn-default">
                                                <a href="https://www.waze.com/ul?ll=-25.384988%2C-51.467499&navigate=yes" target="_blank" rel="noreferrer"><i class="fab fa-waze"></i>Ver no Waze</a>
                                            </div>
                                            <div class="btn-default">
                                                <a href="https://www.google.com/maps/place/'+ location.address +'" target="_blank"><i class="fas fa-map-marker-alt"></i>Ver no Google Maps</a>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="map">
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3604.5909874116924!2d-51.469731049821554!3d-25.38502098372972!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ef36360c7a9cdb%3A0x55542906c2c26857!2sK13%20Ag%C3%AAncia%20Web!5e0!3m2!1spt-BR!2sbr!4v1592853350935!5m2!1spt-BR!2sbr" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="row row-cols-1 row-cols-sm-2">
                                        <div class="col">
                                            <h2 class="address--name">Filial 2</h2>
                                            <address>
                                                <a href="https://api.whatsapp.com/send?phone=5542999999999" target="_blank" rel="noreferrer"><i class="fab fa-whatsapp"></i>42 9 9494 9494</a>
                                                <a href="tel:4233333333"><i class="fas fa-phone"></i>42 3636 3636</a>
                                                <a href="https://www.instagram.com/" target="_blank" rel="noreferrer"><i class="fab fa-instagram"></i>/user</a>
                                                <a href="https://facebook.com/" target="_blank" rel="noreferrer"><i class="fab fa-facebook-square"></i>/user</a>
                                                <a href="mailto:email@email.com"><i class="fas fa-envelope"></i>email@email.com</a>
                                                <p><i class="fas fa-map-marker-alt"></i>Av. Rua Endereço, 4242</p>
                                                <p>Guarapuava/PR</p>
                                            </address>
                                            <div class="btn-default">
                                                <a href="https://www.waze.com/ul?ll=-25.384988%2C-51.467499&navigate=yes" target="_blank" rel="noreferrer"><i class="fab fa-waze"></i>Ver no Waze</a>
                                            </div>
                                            <div class="btn-default">
                                                <a href="https://www.google.com/maps/place/'+ location.address +'" target="_blank"><i class="fas fa-map-marker-alt"></i>Ver no Google Maps</a>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="map">
                                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3604.216713094448!2d-51.47385914982125!3d-25.397555483723924!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ef362d21d8dde9%3A0x1be12283c4158d16!2sParque%20do%20Lago!5e0!3m2!1spt-BR!2sbr!4v1592854695808!5m2!1spt-BR!2sbr" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slider-buttons">
                                <button class="btn-prev"><i class="fas fa-angle-left"></i></button>
                                <button class="btn-next"><i class="fas fa-angle-right"></i></button>
                            </div>
                        </div>
                        <div class="contact-form">
                            <div class="page">
                                <p>Entre em contato através do WhatsApp, ou enviando uma mensagem através do formulário abaixo</p>
                            </div>
                            <div class="btn-social">
                                <a class="whats" href="https://api.whatsapp.com/send?phone=5542994949494" target="_blank" rel="noreferrer">
                                    <i class="fab fa-whatsapp"></i>
                                    <span>Enviar WhatsApp</span>
                                </a>
                            </div>
                            <h2 class="title text-uppercase">Enviar Mensagem</h2>
                            <form action="" method="POST" class="form-layout">
                                <!--
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    Contato enviado com sucesso!
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    ERRO! Preencha os campos corretamente.
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                -->
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="name">Nome</label>
                                            <input type="text" name="name" id="name" required="required" />
                                        </div>
                                        <div class="form-group">
                                            <label for="email">E-mail</label>
                                            <input type="email" name="email" id="email" required="required" />
                                        </div>
                                        <div class="form-group">
                                            <label for="subject">Assunto</label>
                                            <input type="text" name="subject" id="subject" required="required" />
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="message">Mensagem</label>
                                            <textarea name="message" id="message" required="required"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <div class="recaptcha">
                                                <div class="g-recaptcha" data-theme="light" data-sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"></div>
                                            </div>
                                            <button>ENVIAR</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php include ('footer.php'); ?>