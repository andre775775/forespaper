<?php include('header.php'); ?>

    <div id="signup">
        <section class="title bg-full text-center">
            <div class="container">
                <h1>Cadastre-se para fazer suas compras!</h1>
            </div>
        </section>

        <section class="text">
            <div class="container">
                <div class="page">
                    <p>Preencha seus dados no formulário abaixo, ou com sua rede social</p>
                </div>
            </div>
        </section>

        <section class="social-buttons">
            <div class="container">
                <div class="row row-cols-1 row-cols-sm-2">
                    <div class="col">
                        <div class="btn-social">
                            <a class="face" href="#" target="_blank" rel="noreferrer">
                                <i class="fab fa-facebook-square"></i>
                                <span>Cadastro com Facebook</span>
                            </a>
                        </div>
                    </div>
                    <div class="col">
                        <div class="btn-social">
                            <a class="google" href="#" target="_blank" rel="noreferrer">
                                <i class="fab fa-google"></i>
                                <span>Cadastro com Google</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="form">
            <div class="container">
                <form action="" method="POST" class="form-layout">
                    <div class="card">
                        <h2 class="card--title">Informações Pessoais</h2>
                        <div class="form-group">
                            <div class="radio-custom-box person-type">          
                                <div class="item">
                                    <input type="radio" name="person" id="type-1" value="1" class="check-radio" checked/>
                                    <label for="type-1">Pessoa Física</label>
                                </div>
                                <div class="item">
                                    <input type="radio" name="person" id="type-2" value="2" class="check-radio"/>
                                    <label for="type-2">Pessoa Jurídica</label>
                                </div>
                            </div>
                        </div>
                        <div class="physical-person">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="name">Nome</label>
                                        <input name="name" id="name" type="text" required/>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-5 col-md-4">
                                    <div class="form-group">
                                        <label for="cpf">CPF</label>
                                        <input name="cpf" id="cpf" class="cpf-mask" type="tel" placeholder="000.000.000-00" required/>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-4">
                                    <div class="form-group">
                                        <label for="sex">Sexo</label>
                                        <div class="select-custom-box">
                                            <span class="select-label" id="select-sex"><span>Selecione o Sexo</span></span>
                                            <select name="sex" id="sex" class="select-custom" label="select-sex" required>
                                                <option value="">Selecione o Sexo</option>
                                                <option value="F">Feminino</option>
                                                <option value="M">Masculino</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-5 col-md-4">
                                    <div class="form-group">
                                        <label for="birth-date">Data de Nascimento</label>
                                        <input name="birth-date" id="birth-date" class="date-mask" type="tel" required/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="legal-person" style="display: none">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="social-name">Razão Social</label>
                                        <input name="social-name" id="social-name" type="text" required/>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label for="fantasy-name">Nome Fantasia</label>
                                        <input name="fantasy-name" id="fantasy-name" type="text" required/>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="cnpj">CNPJ</label>
                                        <input name="cnpj" id="cnpj" class="cnpj-mask" type="tel" placeholder="000.000.000/0000-00" required/>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-5">
                                    <div class="form-group">
                                        <label for="insc">Inscrição Estadual</label>
                                        <input name="insc" id="insc" class="insc-mask" type="text" onkeyup="this.value = this.value.toUpperCase();" placeholder="00.000.000-0 - XX" required/>
                                    </div>
                                </div>
                                <div class="exempt-btn">
                                    <button type="button" onclick="exemptBtn()" >
                                        <div id="exempt-icon" class="icon"></div>
                                        <span>Isento</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <h2 class="card--title">Seu Endereço</h2>
                        <div class="row">
                            <div class="col-12 col-sm-5">
                                <div class="form-group">
                                    <label for="cep">CEP</label>
                                    <input name="cep" id="cep" class="cep-mask" type="tel" placeholder="00000-000" required/>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <div class="find-cep">
                                        <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank" rel="noreferrer">Não sei meu CEP</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-9">
                                <div class="form-group">
                                    <label for="address">Endereço</label>
                                    <input name="address" id="address" type="text" required>
                                </div>
                            </div>
                            <div class="col-12 col-sm-3 col-md-2">
                                <div class="form-group">
                                    <label for="number">Nº</label>
                                    <input name="number" id="number" class="num-mask" type="tel" required>
                                </div>
                            </div>
                            <div class="col-12 col-sm-9 col-md-8">
                                <div class="form-group">
                                    <label for="complement">Complemento</label>
                                    <input name="complement" id="complement" type="text">
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="form-group">
                                    <label for="neighborhood">Bairro</label>
                                    <input name="neighborhood" id="neighborhood" type="text" required>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4">
                                <div class="form-group">
                                    <label for="telphone">Telefone</label>
                                    <input name="telphone" id="telphone" type="tel" class="tel-mask" placeholder="(00) 0000-0000">
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4">
                                <div class="form-group">
                                    <label for="celphone">Celular</label>
                                    <input name="celphone" id="celphone" type="tel" class="cel-mask" placeholder="(00) 0 0000-0000">
                                </div>
                            </div>
                            <div class="w-100"></div>
                            <div class="col-12 col-sm-4 col-md-3">
                                <div class="form-group">
                                    <label for="estado">Estado</label>
                                    <div class="select-custom-box">
                                        <span class="select-label" id="select-estado">Estado</span>
                                        <select name="estado" id="estado" class="estado select-custom" label="select-estado" required="required"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-8 col-md-6">
                                <div class="form-group">
                                    <label for="city">Cidade</label>
                                    <input name="city" id="city" type="text" required>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="radio-custom-box">          
                                        <div class="item">
                                            <input type="checkbox" name="use-address" id="address-1" class="check-radio"/>
                                            <label for="address-1">Usar esse endereço para envio</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <h2 class="card--title">Seu Login</h2>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group icon">
                                    <label for="email">E-mail</label>
                                    <input name="email" id="email" type="email" placeholder="exemplo@exemplo.com" required>
                                    <i class="far fa-envelope"></i>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    <label for="password">Senha</label>
                                    <input name="password" id="password" type="password" required>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    <label for="check-password">Confirmar Senha</label>
                                    <input name="check-password" id="check-password" type="password" required>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="radio-custom-box">          
                                        <div class="item">
                                            <input type="checkbox" name="terms" id="terms" class="check-radio"/>
                                            <label for="terms">Li e concordo com os <a href="#">Termos e Políticas de funcionamento do site</a></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-buttons">
                        <div class="form-group">
                            <div class="recaptcha">
                                <div class="g-recaptcha" data-theme="light" data-sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"></div>
                            </div>
                            <button type="submit">Cadastrar <i class="fas fa-angle-right"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>

<?php include('footer.php'); ?>