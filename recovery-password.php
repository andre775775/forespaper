<?php include('header.php'); ?>

    <div id="recovery-password" class="login-layout">
        <section class="container">
            <div class="box">
                <h1 class="title color-2 text-center">Redefinição de Senha</h1>
                <form action="" method="POST" class="form-layout">
                    <!--
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        ERRO! As senhas não coincidem.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    -->
                    <div class="form-group">
                        <label for="recovery-password">Sua nova senha</label>
                        <input name="recovery-password" id="recovery-password" type="password" required>
                    </div>
                    <div class="form-group">
                        <label for="confirm-recovery-password">Confirme sua nova senha</label>
                        <input name="confirm-recovery-password" id="confirm-recovery-password" type="password" required>
                    </div>

                    <div class="form-group form-buttons">
                        <div class="recaptcha">
                            <div class="g-recaptcha" data-theme="light" data-sitekey="6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"></div>
                        </div>
                        <button type="submit">REDEFINIR SENHA</button>
                    </div>
                </form>
            </div>
        </section>
        <section class="container">
            <div class="recovery-success">
                <p class="title">Senha redefinida com sucesso!</p>
                <div class="btn-default btn-border">
                    <a href="login.php"><i class="fas fa-angle-left"></i> Voltar para login</a>
                </div>
            </div>
        </section>
    </div>

<?php include('footer.php'); ?>