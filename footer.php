        </div>
        <footer id="footer">
        	<button class="scroll-top">
				<i class="fas fa-caret-up" aria-hidden></i>
				<span class="hidden-text">Voltar ao topo</span>
			</button>
    		<nav class="menu-footer">
    			<div class="container">
    				<div class="row">
						<div class="item col-12 col-md-7 col-lg-5">
							<h4 class="tit text-uppercase">Ajuda/Central de Relacionamento</h4>
							<ul class="two-columns">
								<li><a href="page.php">Quem Somos</a></li>
								<li><a href="contact.php">Contatos/Localização</a></li>
								<li><a href="page.php">Trocas e Devoluções</a></li>
								<li><a href="page.php">Prazos de Entrega</a></li>
								<li><a href="page.php">Políticas e Privacidade</a></li>
								<li><a href="cart.php">Formas de Pagamento</a></li>
							</ul>
						</div>
						<div class="item col-12 col-sm-6 col-md-5 col-lg-3">
							<h4 class="tit text-uppercase">Formas de Pagamento</h4>
							<ul class="payment-methods">
								<li>
									<img data-src="imgs/flags/amex.svg" class="lazyload" alt="American Express">
								</li>
								<li>
									<img data-src="imgs/flags/billet.svg" class="lazyload" alt="Boleto Bancário">
								</li>
								<li>
									<img data-src="imgs/flags/dinersclub.svg" class="lazyload" alt="Diners Club">
								</li>
								<li>
									<img data-src="imgs/flags/elo.svg" class="lazyload" alt="Elo">
								</li>
								<li>
									<img data-src="imgs/flags/hipercard.svg" class="lazyload" alt="Hipercard">
								</li>
								<li>
									<img data-src="imgs/flags/mastercard.svg" class="lazyload" alt="Mastercard">
								</li>
								<li>
									<img data-src="imgs/flags/visa.svg" class="lazyload" alt="Visa">
								</li>
								<li>
									<img data-src="imgs/flags/visacheckout.svg" class="lazyload" alt="Visa Checkout">
								</li>
							</ul>
						</div>
						<div class="item col-12 col-sm-6 col-lg-2 col-xl-3">
							<h4 class="tit text-uppercase">Redes Sociais</h4>
							<ul class="social">
								<li><a href="https://facebook.com/" title="Facebook" target="_blank" rel="noreferrer"><i class="fab fa-facebook-square"></i></a></li>
                        		<li><a href="https://twitter.com" title="Twitter" target="_blank" rel="noreferrer"><i class="fab fa-twitter"></i></a></li>
								<li><a href="https://www.instagram.com/" title="Instagram" target="_blank" rel="noreferrer"><i class="fab fa-instagram"></i></a></li>
							</ul>
						</div>
    				</div>
    			</div>
    		</nav>
					
			<div class="address">
				<div class="container">
					<div class="row">
						<div class="col-12 col-sm-4 col-md-3 col-lg-2">
							<div class="logo">
								<img srcset="imgs/logo.png, imgs/logo2x.png" src="imgs/logo.png" alt="Base Ecommerce Essencial"/>
							</div>
						</div>
						<div class="col-12 col-sm-7 col-md-8 col-lg-9" id="footeraddress">
							<address>
								<p>CNPJ: 99.999.999.9999.99</p>
								<p>
									<a href="https://api.whatsapp.com/send?phone=5542999999999" title="Whatsapp" target="_blank" rel="noreferrer">
										<i class="fab fa-whatsapp"></i>
										<span>42 9 9494 9494</span>
									</a>
									<a href="tel:4236363636">
										<i class="fas fa-phone"></i>
										<span>42 3636 3636</span>
									</a>
								</p>
								<p>Rua Benjamin Constant, 99 - Guarapuava/PR</p>
							</address>
							<div class="k13">
								<a href="https://k13.com.br/" target="_blank" rel="noreferrer" title="K13 - Agência Web"><img src="imgs/k13.png" alt="K13 - Agência Web"/></a>
								<p>- Todos os direitos reservados</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		
		<?php include('libs/btn-whatsapp/components/btn-whatsapp.php') ?>

		<script src="libs/jquery-3.5.1.min.js"></script>
        <script src="libs/bootstrap-4.5.0/js/bootstrap.bundle.min.js"></script>
        <script src="libs/fancybox-3.5.7/dist/jquery.fancybox.min.js"></script>
				<script src="libs/lazysizes-5.3.2.min.js"></script>
		<script src="libs/slick-slider-1.8.1/slick/slick.min.js"></script>
		<script src="libs/simplebar/simplebar.min.js"></script>
        <script src="libs/jquery.mask.js"></script>
		<script src="libs/btn-whatsapp/dist/js/base.js" data-name="k13-whatsapp" data-position-icon-x="right" data-position-icon-y="68" data-position-content-y="20"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
		<script src="libs/elevatezoom/jquery.elevateZoom-3.0.8.min.js"></script>
		<?php if (strpos($_SERVER['REQUEST_URI'], 'product.php')): ?>
			<script src="libs/btn-installments/js/installments.js"></script>
		<?php endif; ?> 
        <script src="js/base.js"></script>
        
    </body>
</html>