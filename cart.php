<?php include('header.php'); ?>

<div id="cart">
    <section class="title bg-full text-center">
        <div class="container">
            <h1>Carrinho</h1>
        </div>
    </section>
    <section class="cart">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="product">
                        <div class="list-prod">
                            <div class="item">
                                <a href="product.php" class="thumb">
                                    <img data-src="imgs/products/product-01.png" class="img-cover lazyload" alt="">
                                </a>
                                <div class="info">
                                    <div class="box">
                                        <h2 class="name"><strong>Produto 01</strong></h2>
                                        <ul class="optional list-inline">
                                            <li class="list-inline-item">Tamanho <strong>X</strong></li>
                                            <li class="list-inline-item">Cor <strong>Y</strong></li>
                                        </ul>
                                    </div>
                                    <div class="foot">
                                        <div class="qtt-box">
                                            <div class="number">
                                                <input class="number__field num-mask" type="tel" id="number1" min="1" max="999" step="1" value="1">
                                            </div>
                                            <span>Quantidade</span>
                                        </div>
                                        <div class="total">
                                            <span class="subtotal">Subtotal</span>
                                            <span class="price"><strong>R$1.499,90</strong></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="buttons">
                                    <div class="btn-default">
                                        <a href="#"><i class="fas fa-trash"></i><span>Remover</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <a href="product.php" class="thumb">
                                    <img data-src="imgs/products/product-02.png" class="img-cover lazyload" alt="">
                                </a>
                                <div class="info">
                                    <div class="box">
                                        <h2 class="name"><strong>Produto 02</strong></h2>
                                        <ul class="optional list-inline">
                                            <li class="list-inline-item">Tamanho <strong>X</strong></li>
                                            <li class="list-inline-item">Cor <strong>Y</strong></li>
                                        </ul>
                                    </div>
                                    <div class="foot">
                                        <div class="qtt-box">
                                            <div class="number">
                                                <input class="number__field num-mask" type="tel" id="number1" min="1" max="999" step="1" value="1">
                                            </div>
                                            <span>Quantidade</span>
                                        </div>
                                        <div class="total">
                                            <span class="subtotal">Subtotal</span>
                                            <span class="price"><strong>R$149,90</strong></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="buttons">
                                    <div class="btn-default">
                                        <a href="#"><i class="fas fa-trash"></i><span>Remover</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="data">
                        <div class="row">
                            <div class="col-md-6 col-lg-12">
                                <h3 class="title"><strong>Consulte o valor e prazo de entrega</strong></h3>
                                <form action="" method="" class="form-layout-cart">
                                    <input type="tel" name="cep" id="cep" class="cep-mask" required="required" placeholder="ex: 99999-999" />
                                    <button type="submit">Consultar</button>
                                </form>
                                <div class="delivery-methods">
                                    <div class="radio-custom-box">          
                                        <span>Entrega Rápida - Guarapuava PR</span>
                                        <div class="item">
                                            <input type="radio" name="delivery" id="method-1" class="check-radio" />
                                            <label for="method-1">Entrega Rápida - R$5,00</label>
                                        </div>
                                        <span>Retirar no Local</span>
                                        <div class="item">
                                            <input type="radio" name="delivery" id="method-2" class="check-radio" />
                                            <label for="method-2">Retirar no Local - GRÁTIS</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-12">
                                <h3 class="title"><strong>Cupom de desconto:</strong></h3>
                                <form action="" method="" class="form-layout-cart">
                                    <input type="text" name="cupom" id="cupom" required="required" />
                                    <button type="submit">OK</button>
                                </form>
                            </div>
                            <div class="col-lg-12">
                                <h3 class="title big"><strong>Totais</strong></h3>
                                <table>
                                    <tr>
                                        <td><p>Subtotal (4 itens)</p></td>
                                        <td><span>R$149,90</span></td>
                                    </tr>
                                    <tr>
                                        <td><p>Desconto</p></td>
                                        <td><span>R$19,80</span></td>
                                    </tr>
                                    <tr>
                                        <td><p>Frete</p></td>
                                        <td><span>R$19,80</span></td>
                                    </tr>
                                    <tr>
                                        <td><p>Total</p></td>
                                        <td>
                                            <span class="big">R$1.999,90</span>
                                            <p class="installments">em até 12x no cartão</p>
                                            <div class="billet">
                                                <p>R$99,90 à vista no Boleto</p>
                                                <img data-src="imgs/flags/billet.svg" class="lazyload" alt="Boleto Bancário">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div class="btn-default" id="finalizar">
                                    <button type="button"><strong>FINALIZAR PEDIDO</strong></button>
                                </div>
                                <div class="btn-cart-box">
                                    <div class="btn-cart">
                                        <a href="#">Continuar<br/>comprando</a>
                                    </div>
                                    <div class="btn-cart gray">
                                        <a href="#">Limpar<br/>carrinho</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include('footer.php'); ?>