jQuery(function($) {
    jQuery(document).ready(function() {
        $(window).resize(function() {
            if ($('#btn-installments').parent().width() > 360) {
                $('#installments').addClass('columns-2');
            } else $('#installments').removeClass('columns-2');
        });
        $(window).trigger('resize');
    });
});