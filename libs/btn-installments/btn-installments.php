
<button id="btn-installments" class="collapsed" type="button" data-toggle="collapse" data-target="#installments" aria-expanded="false" aria-controls="collapseExample">
    Ver Parcelas
</button>

<div id="installments" class="collapse">
    <h3 id="installment"><i class="fas fa-credit-card"></i> Parcelamento</h3>
    <ul id="values">
        <li>1x R$150,00 <span class="interest-free">s/ juros</span></li>
        <li>2x R$75,00  <span class="interest-free">s/ juros</span></li>
        <li>3x R$50,00  <span class="interest-free">s/ juros</span></li>
        <li>4x R$37,50  <span class="interest-free">s/ juros</span></li>
        <li>5x R$30,00  <span class="interest-free">s/ juros</span></li>
        <li>6x R$25,75 c/ juros </li>
        <li>7x R$22,40 c/ juros </li>
        <li>8x R$19,88 c/ juros </li>
        <li>9x R$17,93 c/ juros </li>
        <li>10x R$16,37 c/ juros </li>
    </ul>
</div>