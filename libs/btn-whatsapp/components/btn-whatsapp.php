<div class="whatsapp-box">
	<span class="whatsapp-icon" id="icon-wpp" data-href=""></span>
	<div class="wpp-content">
		<span class="wpp-close" id="wppClose"></span>
		<div class="wpp-head">
			<p>Converse pelo Whatsapp</p>
			<i class="wpp-icon"></i>
		</div>
		<p class="wpp-sub-tit">Entraremos em contato assim que possível.</p>
		<div class="wpp-itens">
			<a href="https://api.whatsapp.com/send?phone=5542999999999" target="_blank" class="wpp-item">
				<i class="wpp-icon green"></i>
				<div class="wpp-txt">
					<p>Comercial</p>
					<span>Fale com o setor comercial</span>
				</div>
			</a>
			<a href="https://api.whatsapp.com/send?phone=5542999999999" target="_blank" class="wpp-item">
				<i class="wpp-icon green"></i>
				<div class="wpp-txt">
					<p>Financeiro</p>
					<span>Fale com o setor financeiro</span>
				</div>
			</a>
			<a href="https://api.whatsapp.com/send?phone=5542999999999" target="_blank" class="wpp-item">
				<i class="wpp-icon green"></i>
				<div class="wpp-txt">
					<p>Suporte</p>
					<span>Fale com o setor suporte</span>
				</div>
			</a>
		</div>
	</div>
</div>