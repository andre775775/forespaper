jQuery(document).ready(function(){
    jQuery('#icon-wpp, #wppClose').click(function(){
        var href = jQuery(this).data('href');
        if(href){
            window.open(href);
            return;
        }
        jQuery('.whatsapp-box').toggleClass('active');
    });
});

var me = document.querySelector('script[data-name="k13-whatsapp"]');
if(me){
    var dataPositionIconX = me.getAttribute('data-position-icon-x');
    var dataPositionIconY = me.getAttribute('data-position-icon-y');
    var dataPositionContentY = me.getAttribute('data-position-content-y');

    if(dataPositionIconX == 'left'){
        jQuery('.whatsapp-box').addClass('left');
    }

    jQuery('.whatsapp-box').css('bottom', dataPositionIconY + 'px');
    jQuery('.whatsapp-box .wpp-content').css('bottom', dataPositionContentY + 'px');
}